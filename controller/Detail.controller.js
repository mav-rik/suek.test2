sap.ui.define(['sap/ui/core/mvc/Controller'],
    function (Controller) {
        return Controller.extend("suek.test2.controller.Detail", {

            /**
             * Инициализация контроллера
             * Подписываемся на событие роутера
             */
            onInit: function () {
                this.getRouter().getRoute("detail").attachMatched(this.onRouteMatched.bind(this));
            },

            /**
             * Возвращает роутер
             * @returns {*}
             */
            getRouter: function () {
                return sap.ui.core.UIComponent.getRouterFor(this);
            },

            /**
             * Хендлер события роутера
             * Обновляем данные модели
             */
            onRouteMatched: function (oEvent) {
                var data = oEvent.getParameter('arguments');
                var oModel = this.getView().getModel();
                var companies = oModel.getProperty('/companies');
                var currentCompany = companies.find(function (company) {
                    return company.id === data.companyId
                });
                var currentBranch = currentCompany.branches.find(function (branch) {
                    return branch.id == data.branchId
                });
                oModel.setProperty('/currentCompany', currentCompany);
                oModel.setProperty('/currentBranch', currentBranch);
            },

            /**
             * Навигация на 1ю страницу
             */
            onNavBack() {
                this.getRouter().navTo('home');
            },

            /**
             * Навигация в филиал
             * @param oEvent
             */
            onOpenBranch(oEvent) {
                var branchId = oEvent.getParameter('selectedItem').getBindingContext().getObject().id;
                var oModel = this.getView().getModel();
                var currentCompany = oModel.getProperty('/currentCompany');
                this.getRouter().navTo('detail', {
                    companyId: currentCompany.id,
                    branchId: branchId
                })
            },

            /**
             * Инициализация и показ поповера настроек полей
             * @param oEvent
             */
            onShowSettings: function (oEvent) {
                if (!this._oPopover) {
                    this._oPopover = sap.ui.xmlfragment("suek.test2.fragment.fields", this);
                    this.getView().addDependent(this._oPopover);
                    var oModel = this.getView().getModel();
                    var fieldItems = [];
                    var tabConf = oModel.getProperty('/tabConf');
                    for (var name in tabConf) {
                        fieldItems.push({
                            key: name,
                            text: this.readText(name),
                            value: tabConf[name]
                        });
                    }
                    oModel.setProperty('/fieldItems', fieldItems);
                }

                this._oPopover.openBy(oEvent.getSource());
            },

            /**
             * Реагируем на изменение списка показываемых полей
             * @param oEvent
             */
            onSelectionChange: function(oEvent) {
                var oModel = this.getView().getModel();
                var fieldItems = oModel.getProperty('/fieldItems');

                fieldItems.forEach(function(item) {
                    oModel.setProperty('/tabConf/'+item.key, item.value);
                })
            },

            /**
             * Шорткат для чтения языкозависимых текстов ресурсов
             * @param name
             * @param args
             * @returns {*}
             */
            readText: function (name, args) {
                return this.getView().getModel('i18n').getResourceBundle().getText(name, args);
            }

        });
    });