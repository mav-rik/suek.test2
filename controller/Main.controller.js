sap.ui.define(['sap/ui/core/mvc/Controller'],
    function (Controller) {
        return Controller.extend("suek.test2.controller.Main", {

            /**
             * Инициализация контроллера
             * Подписываемся на событие роутера
             */
            onInit: function () {
                this.getRouter().getRoute("home").attachMatched(this.onRouteMatched.bind(this));
            },

            /**
             * Возвращает роутер
             * @returns {*}
             */
            getRouter: function () {
                return sap.ui.core.UIComponent.getRouterFor(this);
            },

            /**
             * Хендлер события роутера
             */
            onRouteMatched: function () {

            },

            /**
             * Реагируем на поле поиска
             * @param oEvent
             */
            onSearch: function (oEvent) {
                var text = oEvent.getSource().getValue();
                this.filterCompanies(text);
            },

            /**
             * Применить фильтры к списку компаний
             * @param text
             */
            filterCompanies: function (text) {
                var table = this.getView().byId('idCompaniesTable');
                var binding = table.getBinding('items');
                if (text) {
                    binding.filter(new sap.ui.model.Filter([
                        new sap.ui.model.Filter('company', 'Contains', text),
                        new sap.ui.model.Filter('ceo', 'Contains', text)
                    ]))
                } else {
                    binding.filter()
                }
            },

            /**
             * Навигация на 2ю страницу при выборе строки
             * @param oEvent
             */
            onSelect: function (oEvent) {
                this.getRouter().navTo('detail', {
                    companyId: oEvent.getParameter('listItem').getBindingContext().getObject().id,
                    branchId: 0
                })
            }

        });
    });