sap.ui.define(['sap/ui/base/Object'],
    function (Object) {

        /**
         * @class CompaniesData выдает данные
         * @extends sap.ui.base.Object
         * @author Artem Maltsev
         * @public
         * @alias suek.test1.model.CompaniesData
         */
        var CompaniesData = Object.extend("suek.test1.model.CompaniesData", {
           constructor: function() {

           }
        });

        /**
         * Чтение данных (можно заменить на вызов api-сервера)
         * @returns {Promise<any>}
         */
        CompaniesData.prototype.read = function () {
            return new Promise(function(resolve, reject) {
                resolve(
                    [
                        {
                            "id": "5c90b06202fbdc3ad2beea11",
                            "company": "Electonic",
                            "city": "Dorneyville",
                            "totals": "$587,684.26",
                            "lastYear": "$19,555.39",
                            "currentYear": "$13,967.55",
                            "ceo": "Jodie Vargas",
                            "branches": [
                                {
                                    "id": 0,
                                    "name": "Accusage",
                                    "balance": "$1,966.88",
                                    "lastYear": "$442.45",
                                    "currentYear": "$692.49",
                                    "partner": false,
                                    "chief": "Boyer Jordan",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Phillips Osborne",
                                            "position": "Gardener",
                                            "age": 26,
                                            "tall": 161,
                                            "weight": 57,
                                            "rating": 8,
                                            "salary": "$322.61"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Cervantes Lindsay",
                                            "position": "Electrician",
                                            "age": 26,
                                            "tall": 192,
                                            "weight": 63,
                                            "rating": 10,
                                            "salary": "$253.50"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Margret Snow",
                                            "position": "Engineer",
                                            "age": 25,
                                            "tall": 159,
                                            "weight": 89,
                                            "rating": 8,
                                            "salary": "$348.49"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Chelsea Shaw",
                                            "position": "Engineer",
                                            "age": 32,
                                            "tall": 174,
                                            "weight": 96,
                                            "rating": 9,
                                            "salary": "$374.87"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Bolton Vasquez",
                                            "position": "Plumber",
                                            "age": 21,
                                            "tall": 160,
                                            "weight": 67,
                                            "rating": 9,
                                            "salary": "$326.20"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Camille Kaufman",
                                            "position": "Gardener",
                                            "age": 18,
                                            "tall": 169,
                                            "weight": 78,
                                            "rating": 7,
                                            "salary": "$480.54"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Villarreal Reilly",
                                            "position": "Electrician",
                                            "age": 37,
                                            "tall": 185,
                                            "weight": 84,
                                            "rating": 8,
                                            "salary": "$242.23"
                                        }
                                    ]
                                },
                                {
                                    "id": 1,
                                    "name": "Hinway",
                                    "balance": "$789.10",
                                    "lastYear": "$562.89",
                                    "currentYear": "$129.47",
                                    "partner": true,
                                    "chief": "Fry Craig",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Marta Stein",
                                            "position": "Gardener",
                                            "age": 43,
                                            "tall": 170,
                                            "weight": 79,
                                            "rating": 3,
                                            "salary": "$328.97"
                                        },
                                        {
                                            "id": 1,
                                            "name": "June Randall",
                                            "position": "Worker",
                                            "age": 48,
                                            "tall": 158,
                                            "weight": 96,
                                            "rating": 7,
                                            "salary": "$323.28"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Patrica Downs",
                                            "position": "Engineer",
                                            "age": 21,
                                            "tall": 185,
                                            "weight": 95,
                                            "rating": 9,
                                            "salary": "$465.17"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Bessie Sanchez",
                                            "position": "Worker",
                                            "age": 27,
                                            "tall": 166,
                                            "weight": 82,
                                            "rating": 5,
                                            "salary": "$379.61"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Porter Serrano",
                                            "position": "Gardener",
                                            "age": 47,
                                            "tall": 158,
                                            "weight": 81,
                                            "rating": 3,
                                            "salary": "$455.73"
                                        }
                                    ]
                                },
                                {
                                    "id": 2,
                                    "name": "Zilphur",
                                    "balance": "$4,921.74",
                                    "lastYear": "$158.52",
                                    "currentYear": "$731.47",
                                    "partner": false,
                                    "chief": "Brandie Carver",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Corrine Bell",
                                            "position": "Gardener",
                                            "age": 45,
                                            "tall": 158,
                                            "weight": 79,
                                            "rating": 5,
                                            "salary": "$224.02"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Lillie Greene",
                                            "position": "Electrician",
                                            "age": 33,
                                            "tall": 190,
                                            "weight": 76,
                                            "rating": 8,
                                            "salary": "$270.58"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Lucia Houston",
                                            "position": "Plumber",
                                            "age": 48,
                                            "tall": 171,
                                            "weight": 71,
                                            "rating": 5,
                                            "salary": "$486.77"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Branch Whitaker",
                                            "position": "Engineer",
                                            "age": 41,
                                            "tall": 159,
                                            "weight": 74,
                                            "rating": 5,
                                            "salary": "$243.43"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Effie Greer",
                                            "position": "Engineer",
                                            "age": 47,
                                            "tall": 161,
                                            "weight": 95,
                                            "rating": 2,
                                            "salary": "$286.99"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Flora Hicks",
                                            "position": "Plumber",
                                            "age": 48,
                                            "tall": 185,
                                            "weight": 60,
                                            "rating": 5,
                                            "salary": "$232.96"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "5c90b062ade1cc1baed9bbb4",
                            "company": "Skyplex",
                            "city": "Barronett",
                            "totals": "$798,040.15",
                            "lastYear": "$10,919.06",
                            "currentYear": "$15,446.64",
                            "ceo": "Bennett Hopkins",
                            "branches": [
                                {
                                    "id": 0,
                                    "name": "Sustenza",
                                    "balance": "$740.81",
                                    "lastYear": "$642.34",
                                    "currentYear": "$52.53",
                                    "partner": false,
                                    "chief": "Eula Good",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Reva Ewing",
                                            "position": "Worker",
                                            "age": 47,
                                            "tall": 169,
                                            "weight": 101,
                                            "rating": 10,
                                            "salary": "$329.94"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Booker Flynn",
                                            "position": "Plumber",
                                            "age": 35,
                                            "tall": 167,
                                            "weight": 72,
                                            "rating": 5,
                                            "salary": "$424.07"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Ola Murphy",
                                            "position": "Electrician",
                                            "age": 25,
                                            "tall": 162,
                                            "weight": 92,
                                            "rating": 10,
                                            "salary": "$273.10"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Deirdre Turner",
                                            "position": "Technician",
                                            "age": 48,
                                            "tall": 173,
                                            "weight": 59,
                                            "rating": 2,
                                            "salary": "$438.21"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Cherie Mcbride",
                                            "position": "Electrician",
                                            "age": 18,
                                            "tall": 162,
                                            "weight": 100,
                                            "rating": 2,
                                            "salary": "$300.90"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Fern Moore",
                                            "position": "Engineer",
                                            "age": 27,
                                            "tall": 176,
                                            "weight": 99,
                                            "rating": 4,
                                            "salary": "$290.63"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Bridgette Tran",
                                            "position": "Gardener",
                                            "age": 46,
                                            "tall": 190,
                                            "weight": 62,
                                            "rating": 8,
                                            "salary": "$467.49"
                                        }
                                    ]
                                },
                                {
                                    "id": 1,
                                    "name": "Myopium",
                                    "balance": "$4,549.84",
                                    "lastYear": "$843.94",
                                    "currentYear": "$369.79",
                                    "partner": true,
                                    "chief": "Nell Green",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Vega Estrada",
                                            "position": "Gardener",
                                            "age": 46,
                                            "tall": 166,
                                            "weight": 80,
                                            "rating": 7,
                                            "salary": "$446.20"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Rosella Talley",
                                            "position": "Worker",
                                            "age": 47,
                                            "tall": 180,
                                            "weight": 76,
                                            "rating": 4,
                                            "salary": "$483.94"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Gamble Gutierrez",
                                            "position": "Technician",
                                            "age": 42,
                                            "tall": 190,
                                            "weight": 67,
                                            "rating": 4,
                                            "salary": "$463.72"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Cassie Pitts",
                                            "position": "Engineer",
                                            "age": 20,
                                            "tall": 154,
                                            "weight": 99,
                                            "rating": 9,
                                            "salary": "$330.13"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Whitney Ware",
                                            "position": "Plumber",
                                            "age": 28,
                                            "tall": 162,
                                            "weight": 61,
                                            "rating": 8,
                                            "salary": "$462.41"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Chan Harper",
                                            "position": "Plumber",
                                            "age": 44,
                                            "tall": 172,
                                            "weight": 67,
                                            "rating": 10,
                                            "salary": "$472.27"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Young Melendez",
                                            "position": "Technician",
                                            "age": 49,
                                            "tall": 185,
                                            "weight": 78,
                                            "rating": 6,
                                            "salary": "$477.27"
                                        },
                                        {
                                            "id": 7,
                                            "name": "Trudy Robinson",
                                            "position": "Electrician",
                                            "age": 47,
                                            "tall": 170,
                                            "weight": 96,
                                            "rating": 3,
                                            "salary": "$232.75"
                                        }
                                    ]
                                },
                                {
                                    "id": 2,
                                    "name": "Baluba",
                                    "balance": "$1,433.72",
                                    "lastYear": "$380.69",
                                    "currentYear": "$47.77",
                                    "partner": false,
                                    "chief": "Howell Berry",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Annette Russo",
                                            "position": "Engineer",
                                            "age": 40,
                                            "tall": 183,
                                            "weight": 56,
                                            "rating": 8,
                                            "salary": "$377.38"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Roy Vaughan",
                                            "position": "Gardener",
                                            "age": 25,
                                            "tall": 161,
                                            "weight": 67,
                                            "rating": 10,
                                            "salary": "$213.29"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Cristina Brown",
                                            "position": "Worker",
                                            "age": 23,
                                            "tall": 169,
                                            "weight": 62,
                                            "rating": 3,
                                            "salary": "$398.82"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Pace Larson",
                                            "position": "Worker",
                                            "age": 19,
                                            "tall": 181,
                                            "weight": 86,
                                            "rating": 2,
                                            "salary": "$231.26"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Mullins Hampton",
                                            "position": "Gardener",
                                            "age": 42,
                                            "tall": 173,
                                            "weight": 60,
                                            "rating": 2,
                                            "salary": "$257.88"
                                        }
                                    ]
                                },
                                {
                                    "id": 3,
                                    "name": "Otherway",
                                    "balance": "$4,078.30",
                                    "lastYear": "$507.13",
                                    "currentYear": "$750.18",
                                    "partner": false,
                                    "chief": "Maryanne Meyers",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Sophie Hays",
                                            "position": "Worker",
                                            "age": 29,
                                            "tall": 185,
                                            "weight": 76,
                                            "rating": 4,
                                            "salary": "$204.89"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Cook Finch",
                                            "position": "Plumber",
                                            "age": 29,
                                            "tall": 158,
                                            "weight": 62,
                                            "rating": 7,
                                            "salary": "$452.60"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Susie Aguilar",
                                            "position": "Worker",
                                            "age": 18,
                                            "tall": 172,
                                            "weight": 66,
                                            "rating": 5,
                                            "salary": "$406.63"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Iris Sargent",
                                            "position": "Worker",
                                            "age": 31,
                                            "tall": 186,
                                            "weight": 100,
                                            "rating": 7,
                                            "salary": "$310.94"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Dudley Whitehead",
                                            "position": "Engineer",
                                            "age": 34,
                                            "tall": 182,
                                            "weight": 61,
                                            "rating": 10,
                                            "salary": "$475.07"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Tania Tillman",
                                            "position": "Plumber",
                                            "age": 23,
                                            "tall": 180,
                                            "weight": 83,
                                            "rating": 6,
                                            "salary": "$231.59"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Fitzpatrick Ayers",
                                            "position": "Gardener",
                                            "age": 41,
                                            "tall": 156,
                                            "weight": 67,
                                            "rating": 3,
                                            "salary": "$346.94"
                                        },
                                        {
                                            "id": 7,
                                            "name": "Warren Bruce",
                                            "position": "Technician",
                                            "age": 30,
                                            "tall": 169,
                                            "weight": 89,
                                            "rating": 10,
                                            "salary": "$370.20"
                                        }
                                    ]
                                },
                                {
                                    "id": 4,
                                    "name": "Solaren",
                                    "balance": "$4,261.53",
                                    "lastYear": "$635.26",
                                    "currentYear": "$782.86",
                                    "partner": true,
                                    "chief": "Chambers Mayo",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Sheri Pena",
                                            "position": "Gardener",
                                            "age": 43,
                                            "tall": 163,
                                            "weight": 98,
                                            "rating": 3,
                                            "salary": "$201.29"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Sloan Delgado",
                                            "position": "Technician",
                                            "age": 47,
                                            "tall": 180,
                                            "weight": 74,
                                            "rating": 3,
                                            "salary": "$427.32"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Annie Emerson",
                                            "position": "Plumber",
                                            "age": 38,
                                            "tall": 166,
                                            "weight": 87,
                                            "rating": 6,
                                            "salary": "$272.46"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Chrystal Craft",
                                            "position": "Electrician",
                                            "age": 31,
                                            "tall": 156,
                                            "weight": 55,
                                            "rating": 9,
                                            "salary": "$376.57"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Todd Roberts",
                                            "position": "Gardener",
                                            "age": 31,
                                            "tall": 179,
                                            "weight": 84,
                                            "rating": 7,
                                            "salary": "$459.78"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Mcconnell Hendrix",
                                            "position": "Worker",
                                            "age": 45,
                                            "tall": 183,
                                            "weight": 83,
                                            "rating": 8,
                                            "salary": "$304.62"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Parker Berger",
                                            "position": "Worker",
                                            "age": 29,
                                            "tall": 168,
                                            "weight": 59,
                                            "rating": 9,
                                            "salary": "$258.14"
                                        },
                                        {
                                            "id": 7,
                                            "name": "Erma Kemp",
                                            "position": "Plumber",
                                            "age": 30,
                                            "tall": 186,
                                            "weight": 83,
                                            "rating": 3,
                                            "salary": "$250.76"
                                        }
                                    ]
                                },
                                {
                                    "id": 5,
                                    "name": "Polaria",
                                    "balance": "$4,397.75",
                                    "lastYear": "$904.48",
                                    "currentYear": "$445.29",
                                    "partner": false,
                                    "chief": "Bianca Rocha",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Durham Mueller",
                                            "position": "Worker",
                                            "age": 25,
                                            "tall": 186,
                                            "weight": 101,
                                            "rating": 9,
                                            "salary": "$443.25"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Chavez Carson",
                                            "position": "Engineer",
                                            "age": 45,
                                            "tall": 165,
                                            "weight": 70,
                                            "rating": 9,
                                            "salary": "$453.88"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Brandi Salinas",
                                            "position": "Worker",
                                            "age": 47,
                                            "tall": 188,
                                            "weight": 76,
                                            "rating": 2,
                                            "salary": "$474.05"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Joni Cote",
                                            "position": "Electrician",
                                            "age": 34,
                                            "tall": 164,
                                            "weight": 60,
                                            "rating": 6,
                                            "salary": "$306.56"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Carmella Kerr",
                                            "position": "Gardener",
                                            "age": 42,
                                            "tall": 161,
                                            "weight": 68,
                                            "rating": 10,
                                            "salary": "$321.54"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "5c90b0627208d3700ce919e7",
                            "company": "Uni",
                            "city": "Clayville",
                            "totals": "$748,165.49",
                            "lastYear": "$16,485.40",
                            "currentYear": "$16,542.15",
                            "ceo": "Livingston Pruitt",
                            "branches": [
                                {
                                    "id": 0,
                                    "name": "Exposa",
                                    "balance": "$3,664.18",
                                    "lastYear": "$402.33",
                                    "currentYear": "$302.25",
                                    "partner": false,
                                    "chief": "Mejia Hayden",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Kemp Dunn",
                                            "position": "Technician",
                                            "age": 28,
                                            "tall": 161,
                                            "weight": 67,
                                            "rating": 2,
                                            "salary": "$234.82"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Hess Brooks",
                                            "position": "Worker",
                                            "age": 48,
                                            "tall": 162,
                                            "weight": 68,
                                            "rating": 3,
                                            "salary": "$477.80"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Hawkins Heath",
                                            "position": "Worker",
                                            "age": 31,
                                            "tall": 185,
                                            "weight": 93,
                                            "rating": 3,
                                            "salary": "$433.37"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Sally Mckinney",
                                            "position": "Technician",
                                            "age": 42,
                                            "tall": 157,
                                            "weight": 58,
                                            "rating": 3,
                                            "salary": "$360.13"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Henrietta Welch",
                                            "position": "Engineer",
                                            "age": 45,
                                            "tall": 164,
                                            "weight": 79,
                                            "rating": 8,
                                            "salary": "$346.89"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Maggie Mcmahon",
                                            "position": "Plumber",
                                            "age": 22,
                                            "tall": 156,
                                            "weight": 97,
                                            "rating": 9,
                                            "salary": "$473.09"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Glenda Kinney",
                                            "position": "Gardener",
                                            "age": 50,
                                            "tall": 172,
                                            "weight": 67,
                                            "rating": 4,
                                            "salary": "$407.50"
                                        },
                                        {
                                            "id": 7,
                                            "name": "Sanders Campos",
                                            "position": "Worker",
                                            "age": 36,
                                            "tall": 186,
                                            "weight": 89,
                                            "rating": 8,
                                            "salary": "$205.15"
                                        }
                                    ]
                                },
                                {
                                    "id": 1,
                                    "name": "Zytrek",
                                    "balance": "$1,028.94",
                                    "lastYear": "$481.30",
                                    "currentYear": "$659.58",
                                    "partner": false,
                                    "chief": "Myra Wilder",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Deanne Todd",
                                            "position": "Technician",
                                            "age": 49,
                                            "tall": 171,
                                            "weight": 67,
                                            "rating": 5,
                                            "salary": "$377.79"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Hansen Gibbs",
                                            "position": "Engineer",
                                            "age": 46,
                                            "tall": 175,
                                            "weight": 77,
                                            "rating": 3,
                                            "salary": "$241.62"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Young Fry",
                                            "position": "Engineer",
                                            "age": 36,
                                            "tall": 157,
                                            "weight": 93,
                                            "rating": 9,
                                            "salary": "$401.56"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Lilian Martin",
                                            "position": "Worker",
                                            "age": 45,
                                            "tall": 174,
                                            "weight": 76,
                                            "rating": 5,
                                            "salary": "$443.91"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Ladonna Wilkerson",
                                            "position": "Worker",
                                            "age": 34,
                                            "tall": 170,
                                            "weight": 73,
                                            "rating": 7,
                                            "salary": "$253.64"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Tonia Le",
                                            "position": "Gardener",
                                            "age": 46,
                                            "tall": 187,
                                            "weight": 76,
                                            "rating": 9,
                                            "salary": "$452.83"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Dixie Bond",
                                            "position": "Plumber",
                                            "age": 40,
                                            "tall": 173,
                                            "weight": 94,
                                            "rating": 8,
                                            "salary": "$276.65"
                                        },
                                        {
                                            "id": 7,
                                            "name": "Glass Harding",
                                            "position": "Engineer",
                                            "age": 36,
                                            "tall": 156,
                                            "weight": 88,
                                            "rating": 3,
                                            "salary": "$430.46"
                                        }
                                    ]
                                },
                                {
                                    "id": 2,
                                    "name": "Comvex",
                                    "balance": "$2,033.23",
                                    "lastYear": "$402.43",
                                    "currentYear": "$58.16",
                                    "partner": false,
                                    "chief": "Hayden Ramirez",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Nunez Townsend",
                                            "position": "Worker",
                                            "age": 45,
                                            "tall": 171,
                                            "weight": 90,
                                            "rating": 6,
                                            "salary": "$461.75"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Huffman Cline",
                                            "position": "Worker",
                                            "age": 26,
                                            "tall": 177,
                                            "weight": 77,
                                            "rating": 8,
                                            "salary": "$464.47"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Rhea Vang",
                                            "position": "Technician",
                                            "age": 22,
                                            "tall": 179,
                                            "weight": 62,
                                            "rating": 7,
                                            "salary": "$286.89"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Walsh Mcfarland",
                                            "position": "Worker",
                                            "age": 35,
                                            "tall": 178,
                                            "weight": 102,
                                            "rating": 9,
                                            "salary": "$224.00"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Lopez Smith",
                                            "position": "Gardener",
                                            "age": 37,
                                            "tall": 175,
                                            "weight": 97,
                                            "rating": 6,
                                            "salary": "$263.06"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Aurora Lynn",
                                            "position": "Electrician",
                                            "age": 26,
                                            "tall": 164,
                                            "weight": 66,
                                            "rating": 8,
                                            "salary": "$244.02"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Kirk Nolan",
                                            "position": "Worker",
                                            "age": 49,
                                            "tall": 181,
                                            "weight": 91,
                                            "rating": 6,
                                            "salary": "$272.45"
                                        }
                                    ]
                                },
                                {
                                    "id": 3,
                                    "name": "Zillidium",
                                    "balance": "$3,299.91",
                                    "lastYear": "$485.01",
                                    "currentYear": "$234.74",
                                    "partner": false,
                                    "chief": "Stuart Dunlap",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Montoya Hernandez",
                                            "position": "Worker",
                                            "age": 22,
                                            "tall": 184,
                                            "weight": 93,
                                            "rating": 10,
                                            "salary": "$445.68"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Norma Hart",
                                            "position": "Gardener",
                                            "age": 47,
                                            "tall": 178,
                                            "weight": 71,
                                            "rating": 9,
                                            "salary": "$267.72"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Gonzalez Savage",
                                            "position": "Electrician",
                                            "age": 39,
                                            "tall": 154,
                                            "weight": 74,
                                            "rating": 9,
                                            "salary": "$210.59"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Ollie Cole",
                                            "position": "Gardener",
                                            "age": 46,
                                            "tall": 183,
                                            "weight": 96,
                                            "rating": 9,
                                            "salary": "$452.26"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Lori Haley",
                                            "position": "Engineer",
                                            "age": 21,
                                            "tall": 186,
                                            "weight": 55,
                                            "rating": 3,
                                            "salary": "$396.70"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Sherrie Sosa",
                                            "position": "Engineer",
                                            "age": 39,
                                            "tall": 160,
                                            "weight": 94,
                                            "rating": 6,
                                            "salary": "$322.88"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Shannon Valenzuela",
                                            "position": "Engineer",
                                            "age": 29,
                                            "tall": 164,
                                            "weight": 63,
                                            "rating": 9,
                                            "salary": "$255.47"
                                        },
                                        {
                                            "id": 7,
                                            "name": "Good Melton",
                                            "position": "Technician",
                                            "age": 39,
                                            "tall": 192,
                                            "weight": 92,
                                            "rating": 7,
                                            "salary": "$454.85"
                                        }
                                    ]
                                },
                                {
                                    "id": 4,
                                    "name": "Applidec",
                                    "balance": "$2,370.14",
                                    "lastYear": "$175.16",
                                    "currentYear": "$205.76",
                                    "partner": false,
                                    "chief": "Robertson Huff",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Crawford Nieves",
                                            "position": "Technician",
                                            "age": 35,
                                            "tall": 166,
                                            "weight": 89,
                                            "rating": 3,
                                            "salary": "$381.60"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Rachel Elliott",
                                            "position": "Worker",
                                            "age": 29,
                                            "tall": 182,
                                            "weight": 76,
                                            "rating": 4,
                                            "salary": "$472.44"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Eileen Deleon",
                                            "position": "Engineer",
                                            "age": 35,
                                            "tall": 161,
                                            "weight": 60,
                                            "rating": 6,
                                            "salary": "$298.47"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Sue Morgan",
                                            "position": "Worker",
                                            "age": 30,
                                            "tall": 188,
                                            "weight": 89,
                                            "rating": 4,
                                            "salary": "$474.10"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Hill Valentine",
                                            "position": "Worker",
                                            "age": 22,
                                            "tall": 165,
                                            "weight": 76,
                                            "rating": 8,
                                            "salary": "$467.62"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "5c90b063732bc63678ce2035",
                            "company": "Codax",
                            "city": "Richmond",
                            "totals": "$265,414.68",
                            "lastYear": "$19,806.57",
                            "currentYear": "$5,609.77",
                            "ceo": "Vance Foley",
                            "branches": [
                                {
                                    "id": 0,
                                    "name": "Tubalum",
                                    "balance": "$182.60",
                                    "lastYear": "$114.04",
                                    "currentYear": "$773.95",
                                    "partner": true,
                                    "chief": "Mack Mcfadden",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Tricia Stark",
                                            "position": "Gardener",
                                            "age": 29,
                                            "tall": 181,
                                            "weight": 79,
                                            "rating": 10,
                                            "salary": "$458.66"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Jimmie Johnston",
                                            "position": "Engineer",
                                            "age": 23,
                                            "tall": 163,
                                            "weight": 61,
                                            "rating": 5,
                                            "salary": "$439.80"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Hopkins Ward",
                                            "position": "Electrician",
                                            "age": 22,
                                            "tall": 166,
                                            "weight": 86,
                                            "rating": 6,
                                            "salary": "$310.06"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Kelly Palmer",
                                            "position": "Electrician",
                                            "age": 41,
                                            "tall": 160,
                                            "weight": 82,
                                            "rating": 3,
                                            "salary": "$496.74"
                                        }
                                    ]
                                },
                                {
                                    "id": 1,
                                    "name": "Grupoli",
                                    "balance": "$617.23",
                                    "lastYear": "$907.49",
                                    "currentYear": "$787.70",
                                    "partner": false,
                                    "chief": "Barbara Parks",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Jodi Sparks",
                                            "position": "Gardener",
                                            "age": 47,
                                            "tall": 186,
                                            "weight": 68,
                                            "rating": 7,
                                            "salary": "$355.73"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Lowe Summers",
                                            "position": "Worker",
                                            "age": 21,
                                            "tall": 185,
                                            "weight": 72,
                                            "rating": 4,
                                            "salary": "$331.58"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Lott Bernard",
                                            "position": "Engineer",
                                            "age": 30,
                                            "tall": 176,
                                            "weight": 79,
                                            "rating": 6,
                                            "salary": "$479.56"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Tonya Crane",
                                            "position": "Engineer",
                                            "age": 33,
                                            "tall": 184,
                                            "weight": 65,
                                            "rating": 6,
                                            "salary": "$346.75"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Leanna Curtis",
                                            "position": "Gardener",
                                            "age": 22,
                                            "tall": 162,
                                            "weight": 91,
                                            "rating": 6,
                                            "salary": "$282.43"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Carey Boone",
                                            "position": "Engineer",
                                            "age": 24,
                                            "tall": 166,
                                            "weight": 77,
                                            "rating": 5,
                                            "salary": "$475.22"
                                        }
                                    ]
                                },
                                {
                                    "id": 2,
                                    "name": "Enquility",
                                    "balance": "$77.97",
                                    "lastYear": "$228.22",
                                    "currentYear": "$549.49",
                                    "partner": false,
                                    "chief": "Collier Hunter",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Lolita Hammond",
                                            "position": "Electrician",
                                            "age": 44,
                                            "tall": 171,
                                            "weight": 98,
                                            "rating": 8,
                                            "salary": "$287.96"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Tammy Quinn",
                                            "position": "Gardener",
                                            "age": 30,
                                            "tall": 161,
                                            "weight": 64,
                                            "rating": 8,
                                            "salary": "$241.80"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Carpenter Key",
                                            "position": "Plumber",
                                            "age": 41,
                                            "tall": 172,
                                            "weight": 67,
                                            "rating": 7,
                                            "salary": "$492.30"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Hull Mccullough",
                                            "position": "Technician",
                                            "age": 42,
                                            "tall": 164,
                                            "weight": 73,
                                            "rating": 9,
                                            "salary": "$361.34"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Dina Jefferson",
                                            "position": "Gardener",
                                            "age": 48,
                                            "tall": 181,
                                            "weight": 60,
                                            "rating": 5,
                                            "salary": "$451.31"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Maribel Dotson",
                                            "position": "Gardener",
                                            "age": 24,
                                            "tall": 161,
                                            "weight": 56,
                                            "rating": 9,
                                            "salary": "$205.52"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Isabelle Soto",
                                            "position": "Worker",
                                            "age": 26,
                                            "tall": 154,
                                            "weight": 93,
                                            "rating": 9,
                                            "salary": "$238.31"
                                        }
                                    ]
                                },
                                {
                                    "id": 3,
                                    "name": "Waretel",
                                    "balance": "$1,146.52",
                                    "lastYear": "$759.45",
                                    "currentYear": "$927.77",
                                    "partner": false,
                                    "chief": "Antoinette Rojas",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Cochran Salazar",
                                            "position": "Gardener",
                                            "age": 22,
                                            "tall": 165,
                                            "weight": 67,
                                            "rating": 3,
                                            "salary": "$268.51"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Caldwell Schneider",
                                            "position": "Gardener",
                                            "age": 29,
                                            "tall": 163,
                                            "weight": 56,
                                            "rating": 5,
                                            "salary": "$302.87"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Shelia Barker",
                                            "position": "Engineer",
                                            "age": 37,
                                            "tall": 167,
                                            "weight": 88,
                                            "rating": 3,
                                            "salary": "$315.21"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Colette Sanders",
                                            "position": "Gardener",
                                            "age": 28,
                                            "tall": 156,
                                            "weight": 92,
                                            "rating": 10,
                                            "salary": "$302.93"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Brewer Garner",
                                            "position": "Engineer",
                                            "age": 26,
                                            "tall": 172,
                                            "weight": 78,
                                            "rating": 3,
                                            "salary": "$336.77"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Fuller Haney",
                                            "position": "Engineer",
                                            "age": 38,
                                            "tall": 176,
                                            "weight": 94,
                                            "rating": 8,
                                            "salary": "$485.00"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Stein Henderson",
                                            "position": "Technician",
                                            "age": 47,
                                            "tall": 159,
                                            "weight": 74,
                                            "rating": 6,
                                            "salary": "$433.89"
                                        },
                                        {
                                            "id": 7,
                                            "name": "Melendez Watkins",
                                            "position": "Engineer",
                                            "age": 25,
                                            "tall": 164,
                                            "weight": 84,
                                            "rating": 6,
                                            "salary": "$491.76"
                                        }
                                    ]
                                },
                                {
                                    "id": 4,
                                    "name": "Quiltigen",
                                    "balance": "$4,497.05",
                                    "lastYear": "$154.96",
                                    "currentYear": "$493.02",
                                    "partner": false,
                                    "chief": "Maryann Torres",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Annmarie Ashley",
                                            "position": "Worker",
                                            "age": 31,
                                            "tall": 179,
                                            "weight": 64,
                                            "rating": 9,
                                            "salary": "$351.21"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Gladys Figueroa",
                                            "position": "Gardener",
                                            "age": 43,
                                            "tall": 183,
                                            "weight": 96,
                                            "rating": 9,
                                            "salary": "$349.85"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Alana Cleveland",
                                            "position": "Technician",
                                            "age": 30,
                                            "tall": 177,
                                            "weight": 97,
                                            "rating": 10,
                                            "salary": "$403.65"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Tamera Malone",
                                            "position": "Gardener",
                                            "age": 47,
                                            "tall": 155,
                                            "weight": 76,
                                            "rating": 7,
                                            "salary": "$362.05"
                                        }
                                    ]
                                },
                                {
                                    "id": 5,
                                    "name": "Veraq",
                                    "balance": "$3,627.42",
                                    "lastYear": "$748.64",
                                    "currentYear": "$142.12",
                                    "partner": true,
                                    "chief": "Molly Taylor",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Blevins Saunders",
                                            "position": "Engineer",
                                            "age": 26,
                                            "tall": 184,
                                            "weight": 73,
                                            "rating": 6,
                                            "salary": "$473.04"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Barton Carroll",
                                            "position": "Worker",
                                            "age": 34,
                                            "tall": 161,
                                            "weight": 56,
                                            "rating": 3,
                                            "salary": "$467.01"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Wiley Donovan",
                                            "position": "Engineer",
                                            "age": 41,
                                            "tall": 166,
                                            "weight": 87,
                                            "rating": 4,
                                            "salary": "$296.08"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Best Burton",
                                            "position": "Electrician",
                                            "age": 21,
                                            "tall": 173,
                                            "weight": 60,
                                            "rating": 4,
                                            "salary": "$322.89"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Rachelle Casey",
                                            "position": "Plumber",
                                            "age": 41,
                                            "tall": 159,
                                            "weight": 66,
                                            "rating": 5,
                                            "salary": "$255.99"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "5c90b063eb1112015d2fd6ee",
                            "company": "Quarex",
                            "city": "Sabillasville",
                            "totals": "$538,587.91",
                            "lastYear": "$6,580.75",
                            "currentYear": "$10,351.46",
                            "ceo": "Ford Randolph",
                            "branches": [
                                {
                                    "id": 0,
                                    "name": "Extro",
                                    "balance": "$234.02",
                                    "lastYear": "$280.98",
                                    "currentYear": "$776.91",
                                    "partner": false,
                                    "chief": "Lindsay Booker",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Evelyn Jennings",
                                            "position": "Plumber",
                                            "age": 26,
                                            "tall": 167,
                                            "weight": 87,
                                            "rating": 8,
                                            "salary": "$495.81"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Jenifer Pacheco",
                                            "position": "Worker",
                                            "age": 48,
                                            "tall": 176,
                                            "weight": 62,
                                            "rating": 8,
                                            "salary": "$428.23"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Consuelo Matthews",
                                            "position": "Worker",
                                            "age": 42,
                                            "tall": 175,
                                            "weight": 100,
                                            "rating": 4,
                                            "salary": "$464.14"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Rosalie Mcleod",
                                            "position": "Plumber",
                                            "age": 48,
                                            "tall": 177,
                                            "weight": 74,
                                            "rating": 10,
                                            "salary": "$218.22"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Holloway Herman",
                                            "position": "Gardener",
                                            "age": 42,
                                            "tall": 187,
                                            "weight": 83,
                                            "rating": 6,
                                            "salary": "$260.43"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Puckett Whitfield",
                                            "position": "Plumber",
                                            "age": 42,
                                            "tall": 172,
                                            "weight": 87,
                                            "rating": 3,
                                            "salary": "$474.99"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Aguilar Hodges",
                                            "position": "Gardener",
                                            "age": 21,
                                            "tall": 167,
                                            "weight": 55,
                                            "rating": 8,
                                            "salary": "$477.17"
                                        },
                                        {
                                            "id": 7,
                                            "name": "Rosanne Hebert",
                                            "position": "Plumber",
                                            "age": 40,
                                            "tall": 167,
                                            "weight": 82,
                                            "rating": 4,
                                            "salary": "$230.97"
                                        }
                                    ]
                                },
                                {
                                    "id": 1,
                                    "name": "Blanet",
                                    "balance": "$1,880.09",
                                    "lastYear": "$391.10",
                                    "currentYear": "$897.17",
                                    "partner": false,
                                    "chief": "Lidia Spencer",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Winifred Terry",
                                            "position": "Electrician",
                                            "age": 44,
                                            "tall": 189,
                                            "weight": 63,
                                            "rating": 8,
                                            "salary": "$497.03"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Sims Ferrell",
                                            "position": "Gardener",
                                            "age": 34,
                                            "tall": 179,
                                            "weight": 80,
                                            "rating": 6,
                                            "salary": "$473.77"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Lacy Booth",
                                            "position": "Worker",
                                            "age": 38,
                                            "tall": 155,
                                            "weight": 90,
                                            "rating": 2,
                                            "salary": "$386.13"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Doyle Sims",
                                            "position": "Gardener",
                                            "age": 39,
                                            "tall": 190,
                                            "weight": 69,
                                            "rating": 5,
                                            "salary": "$364.34"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Earlene Singleton",
                                            "position": "Plumber",
                                            "age": 22,
                                            "tall": 182,
                                            "weight": 91,
                                            "rating": 6,
                                            "salary": "$483.52"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Jody Schroeder",
                                            "position": "Engineer",
                                            "age": 32,
                                            "tall": 170,
                                            "weight": 90,
                                            "rating": 3,
                                            "salary": "$456.41"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Dena Cortez",
                                            "position": "Technician",
                                            "age": 19,
                                            "tall": 186,
                                            "weight": 101,
                                            "rating": 4,
                                            "salary": "$444.35"
                                        },
                                        {
                                            "id": 7,
                                            "name": "Glenn Hale",
                                            "position": "Electrician",
                                            "age": 31,
                                            "tall": 171,
                                            "weight": 83,
                                            "rating": 10,
                                            "salary": "$316.30"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "5c90b0630bf13856817d8106",
                            "company": "Mobildata",
                            "city": "Greenock",
                            "totals": "$306,128.27",
                            "lastYear": "$5,069.23",
                            "currentYear": "$7,439.69",
                            "ceo": "Steele Reid",
                            "branches": [
                                {
                                    "id": 0,
                                    "name": "Glasstep",
                                    "balance": "$236.36",
                                    "lastYear": "$146.25",
                                    "currentYear": "$672.41",
                                    "partner": false,
                                    "chief": "Trevino Gill",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Crane Weeks",
                                            "position": "Worker",
                                            "age": 31,
                                            "tall": 157,
                                            "weight": 82,
                                            "rating": 6,
                                            "salary": "$321.63"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Molina Gillespie",
                                            "position": "Engineer",
                                            "age": 39,
                                            "tall": 181,
                                            "weight": 98,
                                            "rating": 4,
                                            "salary": "$234.93"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Hickman Fernandez",
                                            "position": "Worker",
                                            "age": 35,
                                            "tall": 165,
                                            "weight": 91,
                                            "rating": 3,
                                            "salary": "$222.03"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Ivy Gilmore",
                                            "position": "Technician",
                                            "age": 20,
                                            "tall": 159,
                                            "weight": 78,
                                            "rating": 5,
                                            "salary": "$269.50"
                                        }
                                    ]
                                },
                                {
                                    "id": 1,
                                    "name": "Ewaves",
                                    "balance": "$3,148.84",
                                    "lastYear": "$145.59",
                                    "currentYear": "$208.73",
                                    "partner": false,
                                    "chief": "Myrtle Levy",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Lynch Gray",
                                            "position": "Gardener",
                                            "age": 34,
                                            "tall": 160,
                                            "weight": 80,
                                            "rating": 2,
                                            "salary": "$356.70"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Chandra Foreman",
                                            "position": "Worker",
                                            "age": 47,
                                            "tall": 167,
                                            "weight": 99,
                                            "rating": 6,
                                            "salary": "$260.91"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Camacho Donaldson",
                                            "position": "Engineer",
                                            "age": 50,
                                            "tall": 183,
                                            "weight": 83,
                                            "rating": 10,
                                            "salary": "$230.95"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Harvey Vinson",
                                            "position": "Gardener",
                                            "age": 41,
                                            "tall": 156,
                                            "weight": 70,
                                            "rating": 2,
                                            "salary": "$258.77"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Bauer Stout",
                                            "position": "Worker",
                                            "age": 21,
                                            "tall": 158,
                                            "weight": 100,
                                            "rating": 9,
                                            "salary": "$264.12"
                                        }
                                    ]
                                },
                                {
                                    "id": 2,
                                    "name": "Oceanica",
                                    "balance": "$4,860.94",
                                    "lastYear": "$530.02",
                                    "currentYear": "$252.56",
                                    "partner": false,
                                    "chief": "Fletcher Waters",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Herrera Fischer",
                                            "position": "Worker",
                                            "age": 48,
                                            "tall": 181,
                                            "weight": 94,
                                            "rating": 4,
                                            "salary": "$361.26"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Strong Pittman",
                                            "position": "Engineer",
                                            "age": 39,
                                            "tall": 178,
                                            "weight": 88,
                                            "rating": 6,
                                            "salary": "$313.13"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Henderson Simpson",
                                            "position": "Engineer",
                                            "age": 37,
                                            "tall": 173,
                                            "weight": 67,
                                            "rating": 10,
                                            "salary": "$484.59"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Anastasia Stokes",
                                            "position": "Technician",
                                            "age": 42,
                                            "tall": 176,
                                            "weight": 69,
                                            "rating": 9,
                                            "salary": "$300.98"
                                        }
                                    ]
                                },
                                {
                                    "id": 3,
                                    "name": "Geeky",
                                    "balance": "$2,232.25",
                                    "lastYear": "$673.58",
                                    "currentYear": "$143.79",
                                    "partner": false,
                                    "chief": "Conley Young",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Fowler Stafford",
                                            "position": "Plumber",
                                            "age": 31,
                                            "tall": 158,
                                            "weight": 64,
                                            "rating": 10,
                                            "salary": "$292.66"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Vincent Pierce",
                                            "position": "Electrician",
                                            "age": 28,
                                            "tall": 174,
                                            "weight": 93,
                                            "rating": 10,
                                            "salary": "$295.13"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Merrill Rivers",
                                            "position": "Technician",
                                            "age": 20,
                                            "tall": 179,
                                            "weight": 79,
                                            "rating": 6,
                                            "salary": "$334.17"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Erna Allen",
                                            "position": "Gardener",
                                            "age": 28,
                                            "tall": 157,
                                            "weight": 77,
                                            "rating": 4,
                                            "salary": "$316.01"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Jeanie Bryant",
                                            "position": "Gardener",
                                            "age": 31,
                                            "tall": 163,
                                            "weight": 69,
                                            "rating": 5,
                                            "salary": "$217.18"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Ramsey Sloan",
                                            "position": "Electrician",
                                            "age": 38,
                                            "tall": 162,
                                            "weight": 59,
                                            "rating": 8,
                                            "salary": "$313.28"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Claudine Gates",
                                            "position": "Electrician",
                                            "age": 42,
                                            "tall": 159,
                                            "weight": 101,
                                            "rating": 2,
                                            "salary": "$386.89"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "5c90b0638ed475806d454cbb",
                            "company": "Martgo",
                            "city": "Fedora",
                            "totals": "$671,169.03",
                            "lastYear": "$1,357.91",
                            "currentYear": "$16,816.67",
                            "ceo": "Lillian Fowler",
                            "branches": [
                                {
                                    "id": 0,
                                    "name": "Cosmetex",
                                    "balance": "$2,834.77",
                                    "lastYear": "$797.79",
                                    "currentYear": "$645.43",
                                    "partner": true,
                                    "chief": "Shelley Cotton",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Angelique Knowles",
                                            "position": "Plumber",
                                            "age": 23,
                                            "tall": 188,
                                            "weight": 70,
                                            "rating": 8,
                                            "salary": "$338.72"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Mann Petty",
                                            "position": "Technician",
                                            "age": 40,
                                            "tall": 174,
                                            "weight": 84,
                                            "rating": 10,
                                            "salary": "$494.51"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Laurie Owens",
                                            "position": "Worker",
                                            "age": 47,
                                            "tall": 170,
                                            "weight": 69,
                                            "rating": 5,
                                            "salary": "$307.47"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Bonnie Park",
                                            "position": "Gardener",
                                            "age": 45,
                                            "tall": 166,
                                            "weight": 91,
                                            "rating": 5,
                                            "salary": "$404.46"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Gena Jones",
                                            "position": "Plumber",
                                            "age": 45,
                                            "tall": 169,
                                            "weight": 62,
                                            "rating": 4,
                                            "salary": "$388.57"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Leslie Yang",
                                            "position": "Technician",
                                            "age": 37,
                                            "tall": 192,
                                            "weight": 64,
                                            "rating": 9,
                                            "salary": "$347.24"
                                        }
                                    ]
                                },
                                {
                                    "id": 1,
                                    "name": "Musix",
                                    "balance": "$3,193.38",
                                    "lastYear": "$959.62",
                                    "currentYear": "$922.26",
                                    "partner": true,
                                    "chief": "Bright Mercer",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Parrish Pate",
                                            "position": "Gardener",
                                            "age": 33,
                                            "tall": 169,
                                            "weight": 95,
                                            "rating": 2,
                                            "salary": "$469.08"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Miranda Brady",
                                            "position": "Gardener",
                                            "age": 41,
                                            "tall": 155,
                                            "weight": 93,
                                            "rating": 8,
                                            "salary": "$416.40"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Hogan West",
                                            "position": "Gardener",
                                            "age": 20,
                                            "tall": 175,
                                            "weight": 56,
                                            "rating": 3,
                                            "salary": "$381.27"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Amparo Oneill",
                                            "position": "Engineer",
                                            "age": 30,
                                            "tall": 189,
                                            "weight": 63,
                                            "rating": 4,
                                            "salary": "$294.24"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Watson Mcknight",
                                            "position": "Electrician",
                                            "age": 28,
                                            "tall": 178,
                                            "weight": 93,
                                            "rating": 4,
                                            "salary": "$393.46"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Curtis Cherry",
                                            "position": "Gardener",
                                            "age": 27,
                                            "tall": 174,
                                            "weight": 82,
                                            "rating": 4,
                                            "salary": "$270.58"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Genevieve Miranda",
                                            "position": "Technician",
                                            "age": 43,
                                            "tall": 163,
                                            "weight": 97,
                                            "rating": 4,
                                            "salary": "$464.29"
                                        },
                                        {
                                            "id": 7,
                                            "name": "Concetta Shannon",
                                            "position": "Plumber",
                                            "age": 43,
                                            "tall": 172,
                                            "weight": 66,
                                            "rating": 8,
                                            "salary": "$270.97"
                                        }
                                    ]
                                },
                                {
                                    "id": 2,
                                    "name": "Dogtown",
                                    "balance": "$4,409.92",
                                    "lastYear": "$639.24",
                                    "currentYear": "$35.38",
                                    "partner": true,
                                    "chief": "Linda Davis",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Barron Carlson",
                                            "position": "Engineer",
                                            "age": 27,
                                            "tall": 189,
                                            "weight": 96,
                                            "rating": 2,
                                            "salary": "$353.26"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Wolf Chambers",
                                            "position": "Gardener",
                                            "age": 39,
                                            "tall": 181,
                                            "weight": 76,
                                            "rating": 3,
                                            "salary": "$241.93"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Katherine Lloyd",
                                            "position": "Engineer",
                                            "age": 40,
                                            "tall": 187,
                                            "weight": 72,
                                            "rating": 10,
                                            "salary": "$306.12"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Ashley Oneil",
                                            "position": "Plumber",
                                            "age": 37,
                                            "tall": 166,
                                            "weight": 81,
                                            "rating": 10,
                                            "salary": "$432.54"
                                        }
                                    ]
                                },
                                {
                                    "id": 3,
                                    "name": "Enaut",
                                    "balance": "$1,177.06",
                                    "lastYear": "$945.83",
                                    "currentYear": "$714.38",
                                    "partner": true,
                                    "chief": "Rush Solis",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Schultz Burris",
                                            "position": "Technician",
                                            "age": 46,
                                            "tall": 183,
                                            "weight": 56,
                                            "rating": 5,
                                            "salary": "$343.88"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Dunn Hooper",
                                            "position": "Engineer",
                                            "age": 21,
                                            "tall": 187,
                                            "weight": 96,
                                            "rating": 2,
                                            "salary": "$329.85"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Gale Walsh",
                                            "position": "Gardener",
                                            "age": 40,
                                            "tall": 192,
                                            "weight": 81,
                                            "rating": 3,
                                            "salary": "$478.57"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Donaldson Sweet",
                                            "position": "Electrician",
                                            "age": 50,
                                            "tall": 173,
                                            "weight": 96,
                                            "rating": 8,
                                            "salary": "$271.42"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Carlson Horne",
                                            "position": "Electrician",
                                            "age": 42,
                                            "tall": 161,
                                            "weight": 75,
                                            "rating": 4,
                                            "salary": "$208.95"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "5c90b063e28ff6ce984a8db8",
                            "company": "Enerforce",
                            "city": "Woodlands",
                            "totals": "$544,112.84",
                            "lastYear": "$19,374.28",
                            "currentYear": "$12,378.76",
                            "ceo": "Fanny England",
                            "branches": [
                                {
                                    "id": 0,
                                    "name": "Micronaut",
                                    "balance": "$807.26",
                                    "lastYear": "$557.32",
                                    "currentYear": "$375.84",
                                    "partner": false,
                                    "chief": "Everett Lucas",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Carey Jacobson",
                                            "position": "Plumber",
                                            "age": 23,
                                            "tall": 161,
                                            "weight": 99,
                                            "rating": 4,
                                            "salary": "$291.35"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Foley Macias",
                                            "position": "Plumber",
                                            "age": 41,
                                            "tall": 192,
                                            "weight": 79,
                                            "rating": 4,
                                            "salary": "$267.90"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Alexis Clayton",
                                            "position": "Plumber",
                                            "age": 45,
                                            "tall": 154,
                                            "weight": 63,
                                            "rating": 3,
                                            "salary": "$254.50"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Monica Zimmerman",
                                            "position": "Worker",
                                            "age": 29,
                                            "tall": 156,
                                            "weight": 82,
                                            "rating": 3,
                                            "salary": "$443.82"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Morrow Cooper",
                                            "position": "Worker",
                                            "age": 38,
                                            "tall": 185,
                                            "weight": 97,
                                            "rating": 5,
                                            "salary": "$401.22"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Carissa House",
                                            "position": "Electrician",
                                            "age": 33,
                                            "tall": 181,
                                            "weight": 60,
                                            "rating": 3,
                                            "salary": "$461.34"
                                        }
                                    ]
                                },
                                {
                                    "id": 1,
                                    "name": "Eventage",
                                    "balance": "$3,232.43",
                                    "lastYear": "$37.65",
                                    "currentYear": "$743.92",
                                    "partner": true,
                                    "chief": "Torres Holder",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Cardenas Schultz",
                                            "position": "Plumber",
                                            "age": 50,
                                            "tall": 184,
                                            "weight": 100,
                                            "rating": 4,
                                            "salary": "$262.69"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Mindy Sykes",
                                            "position": "Worker",
                                            "age": 27,
                                            "tall": 172,
                                            "weight": 79,
                                            "rating": 8,
                                            "salary": "$235.69"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Day Decker",
                                            "position": "Engineer",
                                            "age": 39,
                                            "tall": 177,
                                            "weight": 70,
                                            "rating": 2,
                                            "salary": "$374.50"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Jarvis Mitchell",
                                            "position": "Worker",
                                            "age": 34,
                                            "tall": 175,
                                            "weight": 80,
                                            "rating": 10,
                                            "salary": "$234.91"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Taylor Espinoza",
                                            "position": "Technician",
                                            "age": 22,
                                            "tall": 171,
                                            "weight": 95,
                                            "rating": 6,
                                            "salary": "$494.37"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Martina Gonzales",
                                            "position": "Electrician",
                                            "age": 25,
                                            "tall": 156,
                                            "weight": 75,
                                            "rating": 5,
                                            "salary": "$231.68"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Tia Doyle",
                                            "position": "Technician",
                                            "age": 48,
                                            "tall": 164,
                                            "weight": 85,
                                            "rating": 3,
                                            "salary": "$374.80"
                                        }
                                    ]
                                },
                                {
                                    "id": 2,
                                    "name": "Flum",
                                    "balance": "$2,785.66",
                                    "lastYear": "$290.30",
                                    "currentYear": "$443.06",
                                    "partner": true,
                                    "chief": "Zamora Willis",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Leanne Parker",
                                            "position": "Engineer",
                                            "age": 41,
                                            "tall": 159,
                                            "weight": 56,
                                            "rating": 7,
                                            "salary": "$490.66"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Snyder Landry",
                                            "position": "Electrician",
                                            "age": 29,
                                            "tall": 181,
                                            "weight": 96,
                                            "rating": 4,
                                            "salary": "$247.90"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Rojas Benson",
                                            "position": "Technician",
                                            "age": 37,
                                            "tall": 175,
                                            "weight": 68,
                                            "rating": 3,
                                            "salary": "$422.05"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Dunlap Cervantes",
                                            "position": "Worker",
                                            "age": 19,
                                            "tall": 162,
                                            "weight": 92,
                                            "rating": 3,
                                            "salary": "$465.89"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Carter Ellison",
                                            "position": "Gardener",
                                            "age": 47,
                                            "tall": 161,
                                            "weight": 74,
                                            "rating": 10,
                                            "salary": "$210.11"
                                        }
                                    ]
                                },
                                {
                                    "id": 3,
                                    "name": "Ronbert",
                                    "balance": "$2,220.66",
                                    "lastYear": "$581.06",
                                    "currentYear": "$893.14",
                                    "partner": true,
                                    "chief": "Jennie Sharp",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Lynn Beck",
                                            "position": "Gardener",
                                            "age": 32,
                                            "tall": 188,
                                            "weight": 57,
                                            "rating": 10,
                                            "salary": "$365.43"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Francine Briggs",
                                            "position": "Gardener",
                                            "age": 40,
                                            "tall": 181,
                                            "weight": 62,
                                            "rating": 7,
                                            "salary": "$395.85"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Salazar Henson",
                                            "position": "Plumber",
                                            "age": 47,
                                            "tall": 159,
                                            "weight": 59,
                                            "rating": 10,
                                            "salary": "$276.07"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Beulah Rowe",
                                            "position": "Worker",
                                            "age": 36,
                                            "tall": 162,
                                            "weight": 94,
                                            "rating": 10,
                                            "salary": "$432.00"
                                        }
                                    ]
                                },
                                {
                                    "id": 4,
                                    "name": "Vortexaco",
                                    "balance": "$4,751.35",
                                    "lastYear": "$169.70",
                                    "currentYear": "$755.01",
                                    "partner": false,
                                    "chief": "Lucille Myers",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Harrison Hurst",
                                            "position": "Engineer",
                                            "age": 46,
                                            "tall": 156,
                                            "weight": 88,
                                            "rating": 8,
                                            "salary": "$462.26"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Ramos Vazquez",
                                            "position": "Plumber",
                                            "age": 48,
                                            "tall": 167,
                                            "weight": 71,
                                            "rating": 7,
                                            "salary": "$229.68"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Tessa Golden",
                                            "position": "Worker",
                                            "age": 45,
                                            "tall": 156,
                                            "weight": 74,
                                            "rating": 8,
                                            "salary": "$440.47"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Jeanette Parrish",
                                            "position": "Gardener",
                                            "age": 41,
                                            "tall": 173,
                                            "weight": 56,
                                            "rating": 5,
                                            "salary": "$408.62"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Jennifer Luna",
                                            "position": "Electrician",
                                            "age": 32,
                                            "tall": 167,
                                            "weight": 59,
                                            "rating": 7,
                                            "salary": "$273.58"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Carlene Stuart",
                                            "position": "Plumber",
                                            "age": 23,
                                            "tall": 184,
                                            "weight": 82,
                                            "rating": 3,
                                            "salary": "$413.34"
                                        }
                                    ]
                                },
                                {
                                    "id": 5,
                                    "name": "Insuresys",
                                    "balance": "$3,727.41",
                                    "lastYear": "$498.70",
                                    "currentYear": "$885.94",
                                    "partner": true,
                                    "chief": "Letha Horton",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Sampson Carrillo",
                                            "position": "Engineer",
                                            "age": 22,
                                            "tall": 171,
                                            "weight": 68,
                                            "rating": 8,
                                            "salary": "$210.99"
                                        },
                                        {
                                            "id": 1,
                                            "name": "James Slater",
                                            "position": "Plumber",
                                            "age": 18,
                                            "tall": 170,
                                            "weight": 96,
                                            "rating": 6,
                                            "salary": "$387.59"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Rios Ramos",
                                            "position": "Technician",
                                            "age": 34,
                                            "tall": 173,
                                            "weight": 86,
                                            "rating": 9,
                                            "salary": "$236.50"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Mckee Bradley",
                                            "position": "Worker",
                                            "age": 33,
                                            "tall": 187,
                                            "weight": 91,
                                            "rating": 3,
                                            "salary": "$453.93"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "5c90b063b7c6e8a6f40e9c3a",
                            "company": "Protodyne",
                            "city": "Bawcomville",
                            "totals": "$179,409.34",
                            "lastYear": "$2,187.37",
                            "currentYear": "$842.09",
                            "ceo": "Olivia Gould",
                            "branches": [
                                {
                                    "id": 0,
                                    "name": "Xanide",
                                    "balance": "$1,024.30",
                                    "lastYear": "$672.01",
                                    "currentYear": "$755.58",
                                    "partner": true,
                                    "chief": "Lina Blanchard",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Jeanine Hoover",
                                            "position": "Technician",
                                            "age": 30,
                                            "tall": 169,
                                            "weight": 88,
                                            "rating": 8,
                                            "salary": "$206.72"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Owen Mckay",
                                            "position": "Plumber",
                                            "age": 37,
                                            "tall": 188,
                                            "weight": 83,
                                            "rating": 6,
                                            "salary": "$326.50"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Melody Preston",
                                            "position": "Electrician",
                                            "age": 33,
                                            "tall": 180,
                                            "weight": 65,
                                            "rating": 7,
                                            "salary": "$381.09"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Pearlie Hayes",
                                            "position": "Engineer",
                                            "age": 33,
                                            "tall": 166,
                                            "weight": 83,
                                            "rating": 4,
                                            "salary": "$381.79"
                                        },
                                        {
                                            "id": 4,
                                            "name": "White Cummings",
                                            "position": "Technician",
                                            "age": 45,
                                            "tall": 191,
                                            "weight": 79,
                                            "rating": 3,
                                            "salary": "$319.60"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Whitehead Cobb",
                                            "position": "Worker",
                                            "age": 45,
                                            "tall": 189,
                                            "weight": 88,
                                            "rating": 4,
                                            "salary": "$475.38"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Lessie Cain",
                                            "position": "Technician",
                                            "age": 46,
                                            "tall": 156,
                                            "weight": 89,
                                            "rating": 10,
                                            "salary": "$335.26"
                                        },
                                        {
                                            "id": 7,
                                            "name": "Leach Acosta",
                                            "position": "Engineer",
                                            "age": 45,
                                            "tall": 187,
                                            "weight": 83,
                                            "rating": 8,
                                            "salary": "$292.36"
                                        }
                                    ]
                                },
                                {
                                    "id": 1,
                                    "name": "Egypto",
                                    "balance": "$601.47",
                                    "lastYear": "$387.52",
                                    "currentYear": "$661.72",
                                    "partner": true,
                                    "chief": "Moody Stanton",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Ware Silva",
                                            "position": "Plumber",
                                            "age": 50,
                                            "tall": 190,
                                            "weight": 91,
                                            "rating": 7,
                                            "salary": "$205.69"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Fuentes Hall",
                                            "position": "Technician",
                                            "age": 24,
                                            "tall": 186,
                                            "weight": 68,
                                            "rating": 3,
                                            "salary": "$208.13"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Saunders Clements",
                                            "position": "Technician",
                                            "age": 40,
                                            "tall": 171,
                                            "weight": 94,
                                            "rating": 9,
                                            "salary": "$312.33"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Berry Curry",
                                            "position": "Plumber",
                                            "age": 44,
                                            "tall": 181,
                                            "weight": 99,
                                            "rating": 9,
                                            "salary": "$250.95"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Rice Hardin",
                                            "position": "Gardener",
                                            "age": 28,
                                            "tall": 168,
                                            "weight": 66,
                                            "rating": 8,
                                            "salary": "$466.33"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Harper Levine",
                                            "position": "Technician",
                                            "age": 18,
                                            "tall": 190,
                                            "weight": 82,
                                            "rating": 4,
                                            "salary": "$277.91"
                                        }
                                    ]
                                },
                                {
                                    "id": 2,
                                    "name": "Ludak",
                                    "balance": "$4,191.01",
                                    "lastYear": "$736.01",
                                    "currentYear": "$810.35",
                                    "partner": false,
                                    "chief": "Nash Bradshaw",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Mccray Oliver",
                                            "position": "Plumber",
                                            "age": 41,
                                            "tall": 177,
                                            "weight": 102,
                                            "rating": 7,
                                            "salary": "$272.46"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Janna Cross",
                                            "position": "Gardener",
                                            "age": 28,
                                            "tall": 165,
                                            "weight": 102,
                                            "rating": 9,
                                            "salary": "$283.32"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Ethel Schwartz",
                                            "position": "Worker",
                                            "age": 46,
                                            "tall": 174,
                                            "weight": 89,
                                            "rating": 4,
                                            "salary": "$423.83"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Brittney Weiss",
                                            "position": "Gardener",
                                            "age": 48,
                                            "tall": 154,
                                            "weight": 95,
                                            "rating": 7,
                                            "salary": "$324.78"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Stella Harvey",
                                            "position": "Worker",
                                            "age": 33,
                                            "tall": 157,
                                            "weight": 61,
                                            "rating": 9,
                                            "salary": "$228.36"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Scott Rivera",
                                            "position": "Engineer",
                                            "age": 36,
                                            "tall": 191,
                                            "weight": 65,
                                            "rating": 9,
                                            "salary": "$390.99"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Erica Dickson",
                                            "position": "Engineer",
                                            "age": 42,
                                            "tall": 159,
                                            "weight": 102,
                                            "rating": 8,
                                            "salary": "$436.35"
                                        }
                                    ]
                                },
                                {
                                    "id": 3,
                                    "name": "Eventex",
                                    "balance": "$1,455.47",
                                    "lastYear": "$887.59",
                                    "currentYear": "$497.55",
                                    "partner": false,
                                    "chief": "Sheila Case",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Nichols Marsh",
                                            "position": "Gardener",
                                            "age": 23,
                                            "tall": 191,
                                            "weight": 72,
                                            "rating": 2,
                                            "salary": "$335.17"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Luann Carter",
                                            "position": "Plumber",
                                            "age": 40,
                                            "tall": 190,
                                            "weight": 72,
                                            "rating": 5,
                                            "salary": "$464.13"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Roslyn Harrell",
                                            "position": "Technician",
                                            "age": 41,
                                            "tall": 168,
                                            "weight": 101,
                                            "rating": 7,
                                            "salary": "$208.41"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Simon Buck",
                                            "position": "Technician",
                                            "age": 24,
                                            "tall": 181,
                                            "weight": 58,
                                            "rating": 5,
                                            "salary": "$339.56"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Reese Goodman",
                                            "position": "Technician",
                                            "age": 37,
                                            "tall": 155,
                                            "weight": 71,
                                            "rating": 10,
                                            "salary": "$312.00"
                                        }
                                    ]
                                },
                                {
                                    "id": 4,
                                    "name": "Kraggle",
                                    "balance": "$844.43",
                                    "lastYear": "$827.81",
                                    "currentYear": "$19.74",
                                    "partner": true,
                                    "chief": "Deana Norman",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Cherry Robles",
                                            "position": "Gardener",
                                            "age": 34,
                                            "tall": 174,
                                            "weight": 85,
                                            "rating": 8,
                                            "salary": "$462.34"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Pam Hurley",
                                            "position": "Engineer",
                                            "age": 18,
                                            "tall": 180,
                                            "weight": 95,
                                            "rating": 5,
                                            "salary": "$357.19"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Kristina Atkinson",
                                            "position": "Worker",
                                            "age": 36,
                                            "tall": 160,
                                            "weight": 92,
                                            "rating": 2,
                                            "salary": "$296.84"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Cameron Mclean",
                                            "position": "Electrician",
                                            "age": 21,
                                            "tall": 161,
                                            "weight": 98,
                                            "rating": 5,
                                            "salary": "$469.61"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Annabelle George",
                                            "position": "Gardener",
                                            "age": 44,
                                            "tall": 155,
                                            "weight": 83,
                                            "rating": 8,
                                            "salary": "$432.78"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Odonnell Daugherty",
                                            "position": "Worker",
                                            "age": 45,
                                            "tall": 175,
                                            "weight": 72,
                                            "rating": 5,
                                            "salary": "$343.31"
                                        }
                                    ]
                                },
                                {
                                    "id": 5,
                                    "name": "Cubicide",
                                    "balance": "$1,508.04",
                                    "lastYear": "$247.65",
                                    "currentYear": "$179.63",
                                    "partner": true,
                                    "chief": "Holden Chavez",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Michele Lynch",
                                            "position": "Engineer",
                                            "age": 29,
                                            "tall": 169,
                                            "weight": 95,
                                            "rating": 3,
                                            "salary": "$233.75"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Gilliam Wolfe",
                                            "position": "Plumber",
                                            "age": 45,
                                            "tall": 180,
                                            "weight": 97,
                                            "rating": 4,
                                            "salary": "$212.09"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Drake Carr",
                                            "position": "Plumber",
                                            "age": 28,
                                            "tall": 158,
                                            "weight": 91,
                                            "rating": 8,
                                            "salary": "$483.51"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Araceli Owen",
                                            "position": "Electrician",
                                            "age": 35,
                                            "tall": 163,
                                            "weight": 61,
                                            "rating": 9,
                                            "salary": "$210.36"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "5c90b063b4af7a24ffbeb7c5",
                            "company": "Rubadub",
                            "city": "Roderfield",
                            "totals": "$616,042.27",
                            "lastYear": "$13,249.27",
                            "currentYear": "$11,139.45",
                            "ceo": "Reid Mclaughlin",
                            "branches": [
                                {
                                    "id": 0,
                                    "name": "Medmex",
                                    "balance": "$108.92",
                                    "lastYear": "$682.80",
                                    "currentYear": "$489.85",
                                    "partner": true,
                                    "chief": "Simmons Blair",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Mullen Fletcher",
                                            "position": "Engineer",
                                            "age": 42,
                                            "tall": 166,
                                            "weight": 97,
                                            "rating": 2,
                                            "salary": "$473.47"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Fisher Kim",
                                            "position": "Technician",
                                            "age": 23,
                                            "tall": 169,
                                            "weight": 59,
                                            "rating": 4,
                                            "salary": "$497.78"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Mcknight Evans",
                                            "position": "Worker",
                                            "age": 42,
                                            "tall": 185,
                                            "weight": 95,
                                            "rating": 9,
                                            "salary": "$358.74"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Welch Hines",
                                            "position": "Electrician",
                                            "age": 29,
                                            "tall": 181,
                                            "weight": 62,
                                            "rating": 3,
                                            "salary": "$476.03"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Josie Barrera",
                                            "position": "Electrician",
                                            "age": 43,
                                            "tall": 157,
                                            "weight": 73,
                                            "rating": 2,
                                            "salary": "$409.62"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Swanson Hogan",
                                            "position": "Engineer",
                                            "age": 30,
                                            "tall": 167,
                                            "weight": 91,
                                            "rating": 9,
                                            "salary": "$346.70"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Priscilla Conway",
                                            "position": "Engineer",
                                            "age": 43,
                                            "tall": 163,
                                            "weight": 57,
                                            "rating": 9,
                                            "salary": "$341.69"
                                        },
                                        {
                                            "id": 7,
                                            "name": "Peterson Bass",
                                            "position": "Technician",
                                            "age": 18,
                                            "tall": 168,
                                            "weight": 57,
                                            "rating": 8,
                                            "salary": "$260.36"
                                        }
                                    ]
                                },
                                {
                                    "id": 1,
                                    "name": "Comveyor",
                                    "balance": "$3,088.34",
                                    "lastYear": "$393.10",
                                    "currentYear": "$33.82",
                                    "partner": true,
                                    "chief": "Marisol Molina",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Blake Allison",
                                            "position": "Gardener",
                                            "age": 26,
                                            "tall": 178,
                                            "weight": 69,
                                            "rating": 4,
                                            "salary": "$412.31"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Robbie Wright",
                                            "position": "Plumber",
                                            "age": 26,
                                            "tall": 154,
                                            "weight": 73,
                                            "rating": 6,
                                            "salary": "$364.83"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Margarita Hendricks",
                                            "position": "Technician",
                                            "age": 36,
                                            "tall": 192,
                                            "weight": 61,
                                            "rating": 8,
                                            "salary": "$459.03"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Sandoval Perez",
                                            "position": "Electrician",
                                            "age": 28,
                                            "tall": 161,
                                            "weight": 63,
                                            "rating": 6,
                                            "salary": "$317.89"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Hinton Sweeney",
                                            "position": "Technician",
                                            "age": 21,
                                            "tall": 159,
                                            "weight": 74,
                                            "rating": 6,
                                            "salary": "$471.12"
                                        }
                                    ]
                                },
                                {
                                    "id": 2,
                                    "name": "Furnitech",
                                    "balance": "$3,836.35",
                                    "lastYear": "$531.31",
                                    "currentYear": "$242.49",
                                    "partner": true,
                                    "chief": "Thomas Ramsey",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Alison Sawyer",
                                            "position": "Electrician",
                                            "age": 36,
                                            "tall": 158,
                                            "weight": 63,
                                            "rating": 10,
                                            "salary": "$244.32"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Hunter Middleton",
                                            "position": "Engineer",
                                            "age": 23,
                                            "tall": 175,
                                            "weight": 61,
                                            "rating": 6,
                                            "salary": "$497.77"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Monroe Ratliff",
                                            "position": "Plumber",
                                            "age": 32,
                                            "tall": 172,
                                            "weight": 62,
                                            "rating": 4,
                                            "salary": "$468.48"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Soto Rosales",
                                            "position": "Technician",
                                            "age": 18,
                                            "tall": 160,
                                            "weight": 72,
                                            "rating": 3,
                                            "salary": "$259.56"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Riggs Caldwell",
                                            "position": "Worker",
                                            "age": 42,
                                            "tall": 155,
                                            "weight": 76,
                                            "rating": 5,
                                            "salary": "$352.66"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Madeline Chandler",
                                            "position": "Worker",
                                            "age": 18,
                                            "tall": 163,
                                            "weight": 102,
                                            "rating": 5,
                                            "salary": "$209.59"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "5c90b06359740e9a8455e21a",
                            "company": "Acium",
                            "city": "Why",
                            "totals": "$55,944.06",
                            "lastYear": "$11,731.58",
                            "currentYear": "$18,828.05",
                            "ceo": "Elisabeth Jarvis",
                            "branches": [
                                {
                                    "id": 0,
                                    "name": "Lexicondo",
                                    "balance": "$4,329.96",
                                    "lastYear": "$13.26",
                                    "currentYear": "$415.28",
                                    "partner": true,
                                    "chief": "Minnie Browning",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Eleanor Pace",
                                            "position": "Gardener",
                                            "age": 18,
                                            "tall": 157,
                                            "weight": 100,
                                            "rating": 7,
                                            "salary": "$424.25"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Yolanda Vincent",
                                            "position": "Gardener",
                                            "age": 44,
                                            "tall": 175,
                                            "weight": 99,
                                            "rating": 8,
                                            "salary": "$337.19"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Darla Newman",
                                            "position": "Worker",
                                            "age": 42,
                                            "tall": 162,
                                            "weight": 100,
                                            "rating": 4,
                                            "salary": "$383.73"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Stephenson Mcclain",
                                            "position": "Technician",
                                            "age": 44,
                                            "tall": 164,
                                            "weight": 100,
                                            "rating": 5,
                                            "salary": "$287.21"
                                        }
                                    ]
                                },
                                {
                                    "id": 1,
                                    "name": "Endipine",
                                    "balance": "$4,283.28",
                                    "lastYear": "$57.48",
                                    "currentYear": "$135.58",
                                    "partner": true,
                                    "chief": "Susanne Miller",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Klein Obrien",
                                            "position": "Electrician",
                                            "age": 30,
                                            "tall": 176,
                                            "weight": 92,
                                            "rating": 10,
                                            "salary": "$489.84"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Imogene Kane",
                                            "position": "Plumber",
                                            "age": 29,
                                            "tall": 178,
                                            "weight": 66,
                                            "rating": 4,
                                            "salary": "$279.01"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Poole Rodgers",
                                            "position": "Electrician",
                                            "age": 46,
                                            "tall": 168,
                                            "weight": 63,
                                            "rating": 9,
                                            "salary": "$422.63"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Lila Dawson",
                                            "position": "Electrician",
                                            "age": 28,
                                            "tall": 164,
                                            "weight": 75,
                                            "rating": 2,
                                            "salary": "$360.54"
                                        }
                                    ]
                                },
                                {
                                    "id": 2,
                                    "name": "Zork",
                                    "balance": "$2,202.39",
                                    "lastYear": "$735.91",
                                    "currentYear": "$787.98",
                                    "partner": false,
                                    "chief": "David Mcdonald",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Copeland Clay",
                                            "position": "Plumber",
                                            "age": 22,
                                            "tall": 165,
                                            "weight": 58,
                                            "rating": 6,
                                            "salary": "$461.75"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Susan Colon",
                                            "position": "Worker",
                                            "age": 42,
                                            "tall": 161,
                                            "weight": 92,
                                            "rating": 6,
                                            "salary": "$211.69"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Sharon Sandoval",
                                            "position": "Plumber",
                                            "age": 36,
                                            "tall": 159,
                                            "weight": 100,
                                            "rating": 4,
                                            "salary": "$212.01"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Hartman Harrison",
                                            "position": "Electrician",
                                            "age": 25,
                                            "tall": 171,
                                            "weight": 66,
                                            "rating": 6,
                                            "salary": "$212.62"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Clay Manning",
                                            "position": "Worker",
                                            "age": 48,
                                            "tall": 175,
                                            "weight": 58,
                                            "rating": 5,
                                            "salary": "$207.64"
                                        }
                                    ]
                                },
                                {
                                    "id": 3,
                                    "name": "Biotica",
                                    "balance": "$4,637.64",
                                    "lastYear": "$455.74",
                                    "currentYear": "$399.40",
                                    "partner": false,
                                    "chief": "Rosie Flowers",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Cannon Barr",
                                            "position": "Worker",
                                            "age": 35,
                                            "tall": 161,
                                            "weight": 69,
                                            "rating": 10,
                                            "salary": "$321.41"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Brooke Potts",
                                            "position": "Plumber",
                                            "age": 35,
                                            "tall": 169,
                                            "weight": 74,
                                            "rating": 8,
                                            "salary": "$224.36"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Alejandra Nash",
                                            "position": "Worker",
                                            "age": 31,
                                            "tall": 162,
                                            "weight": 55,
                                            "rating": 4,
                                            "salary": "$203.79"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Butler Vaughn",
                                            "position": "Plumber",
                                            "age": 18,
                                            "tall": 187,
                                            "weight": 61,
                                            "rating": 7,
                                            "salary": "$284.22"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Eunice Black",
                                            "position": "Technician",
                                            "age": 19,
                                            "tall": 186,
                                            "weight": 70,
                                            "rating": 10,
                                            "salary": "$382.31"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Sheree Poole",
                                            "position": "Technician",
                                            "age": 43,
                                            "tall": 182,
                                            "weight": 65,
                                            "rating": 10,
                                            "salary": "$447.33"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Jewell Raymond",
                                            "position": "Engineer",
                                            "age": 28,
                                            "tall": 189,
                                            "weight": 96,
                                            "rating": 10,
                                            "salary": "$448.15"
                                        },
                                        {
                                            "id": 7,
                                            "name": "Lucas Madden",
                                            "position": "Engineer",
                                            "age": 27,
                                            "tall": 188,
                                            "weight": 58,
                                            "rating": 8,
                                            "salary": "$485.20"
                                        }
                                    ]
                                },
                                {
                                    "id": 4,
                                    "name": "Kenegy",
                                    "balance": "$3,304.50",
                                    "lastYear": "$248.56",
                                    "currentYear": "$560.40",
                                    "partner": false,
                                    "chief": "Wendi Mcintyre",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Holder Christian",
                                            "position": "Worker",
                                            "age": 35,
                                            "tall": 160,
                                            "weight": 56,
                                            "rating": 9,
                                            "salary": "$413.46"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Harriet Avila",
                                            "position": "Technician",
                                            "age": 47,
                                            "tall": 181,
                                            "weight": 99,
                                            "rating": 4,
                                            "salary": "$289.80"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Lilly Koch",
                                            "position": "Technician",
                                            "age": 47,
                                            "tall": 172,
                                            "weight": 70,
                                            "rating": 2,
                                            "salary": "$255.07"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Battle Holmes",
                                            "position": "Technician",
                                            "age": 28,
                                            "tall": 191,
                                            "weight": 82,
                                            "rating": 2,
                                            "salary": "$226.91"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Enid Beard",
                                            "position": "Technician",
                                            "age": 44,
                                            "tall": 165,
                                            "weight": 55,
                                            "rating": 2,
                                            "salary": "$349.80"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Rosales Burt",
                                            "position": "Electrician",
                                            "age": 37,
                                            "tall": 175,
                                            "weight": 74,
                                            "rating": 5,
                                            "salary": "$211.08"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Mcguire Villarreal",
                                            "position": "Gardener",
                                            "age": 45,
                                            "tall": 182,
                                            "weight": 91,
                                            "rating": 5,
                                            "salary": "$212.83"
                                        },
                                        {
                                            "id": 7,
                                            "name": "Irene Dorsey",
                                            "position": "Engineer",
                                            "age": 42,
                                            "tall": 172,
                                            "weight": 65,
                                            "rating": 3,
                                            "salary": "$343.88"
                                        }
                                    ]
                                },
                                {
                                    "id": 5,
                                    "name": "Zidox",
                                    "balance": "$265.02",
                                    "lastYear": "$696.38",
                                    "currentYear": "$242.13",
                                    "partner": false,
                                    "chief": "Lesley Frazier",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Richard Davidson",
                                            "position": "Worker",
                                            "age": 23,
                                            "tall": 154,
                                            "weight": 73,
                                            "rating": 9,
                                            "salary": "$348.36"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Robinson Baker",
                                            "position": "Electrician",
                                            "age": 20,
                                            "tall": 184,
                                            "weight": 91,
                                            "rating": 8,
                                            "salary": "$399.71"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Melva Clemons",
                                            "position": "Worker",
                                            "age": 40,
                                            "tall": 176,
                                            "weight": 95,
                                            "rating": 2,
                                            "salary": "$297.06"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Reba Aguirre",
                                            "position": "Gardener",
                                            "age": 20,
                                            "tall": 181,
                                            "weight": 76,
                                            "rating": 4,
                                            "salary": "$263.08"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Willie Garrison",
                                            "position": "Gardener",
                                            "age": 38,
                                            "tall": 178,
                                            "weight": 92,
                                            "rating": 2,
                                            "salary": "$453.31"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Loretta Delaney",
                                            "position": "Engineer",
                                            "age": 49,
                                            "tall": 179,
                                            "weight": 60,
                                            "rating": 4,
                                            "salary": "$241.78"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Lacey Rogers",
                                            "position": "Electrician",
                                            "age": 47,
                                            "tall": 167,
                                            "weight": 98,
                                            "rating": 9,
                                            "salary": "$339.24"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "5c90b0639ec35908bcb0372a",
                            "company": "Centree",
                            "city": "Sanborn",
                            "totals": "$512,632.16",
                            "lastYear": "$14,474.22",
                            "currentYear": "$449.89",
                            "ceo": "Katie Woodard",
                            "branches": [
                                {
                                    "id": 0,
                                    "name": "Viocular",
                                    "balance": "$4,757.79",
                                    "lastYear": "$212.08",
                                    "currentYear": "$22.89",
                                    "partner": true,
                                    "chief": "Pratt Bailey",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Carmela Wilkinson",
                                            "position": "Technician",
                                            "age": 23,
                                            "tall": 162,
                                            "weight": 72,
                                            "rating": 5,
                                            "salary": "$402.56"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Barber Blevins",
                                            "position": "Engineer",
                                            "age": 46,
                                            "tall": 188,
                                            "weight": 60,
                                            "rating": 9,
                                            "salary": "$201.30"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Alvarado Mercado",
                                            "position": "Engineer",
                                            "age": 49,
                                            "tall": 176,
                                            "weight": 61,
                                            "rating": 8,
                                            "salary": "$302.20"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Vanessa Dixon",
                                            "position": "Gardener",
                                            "age": 30,
                                            "tall": 166,
                                            "weight": 88,
                                            "rating": 7,
                                            "salary": "$371.98"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Carla Cook",
                                            "position": "Plumber",
                                            "age": 25,
                                            "tall": 158,
                                            "weight": 89,
                                            "rating": 8,
                                            "salary": "$302.78"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Lorrie Merritt",
                                            "position": "Worker",
                                            "age": 38,
                                            "tall": 162,
                                            "weight": 85,
                                            "rating": 10,
                                            "salary": "$464.59"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Therese Branch",
                                            "position": "Technician",
                                            "age": 20,
                                            "tall": 158,
                                            "weight": 56,
                                            "rating": 10,
                                            "salary": "$209.59"
                                        }
                                    ]
                                },
                                {
                                    "id": 1,
                                    "name": "Stockpost",
                                    "balance": "$1,491.44",
                                    "lastYear": "$758.45",
                                    "currentYear": "$551.42",
                                    "partner": true,
                                    "chief": "Terra Crawford",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Compton Murray",
                                            "position": "Worker",
                                            "age": 47,
                                            "tall": 165,
                                            "weight": 76,
                                            "rating": 10,
                                            "salary": "$208.62"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Lee Cochran",
                                            "position": "Technician",
                                            "age": 47,
                                            "tall": 154,
                                            "weight": 89,
                                            "rating": 10,
                                            "salary": "$280.12"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Burch Leblanc",
                                            "position": "Gardener",
                                            "age": 42,
                                            "tall": 189,
                                            "weight": 95,
                                            "rating": 2,
                                            "salary": "$301.47"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Phelps Alford",
                                            "position": "Plumber",
                                            "age": 33,
                                            "tall": 178,
                                            "weight": 88,
                                            "rating": 4,
                                            "salary": "$452.67"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Josefa Mcconnell",
                                            "position": "Electrician",
                                            "age": 34,
                                            "tall": 188,
                                            "weight": 92,
                                            "rating": 9,
                                            "salary": "$403.85"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Garcia Wise",
                                            "position": "Electrician",
                                            "age": 50,
                                            "tall": 173,
                                            "weight": 80,
                                            "rating": 6,
                                            "salary": "$462.78"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Haley English",
                                            "position": "Engineer",
                                            "age": 46,
                                            "tall": 185,
                                            "weight": 86,
                                            "rating": 4,
                                            "salary": "$332.11"
                                        },
                                        {
                                            "id": 7,
                                            "name": "Hammond Hughes",
                                            "position": "Technician",
                                            "age": 31,
                                            "tall": 192,
                                            "weight": 67,
                                            "rating": 10,
                                            "salary": "$233.52"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "5c90b063a87a0ea3b9714c2f",
                            "company": "Luxuria",
                            "city": "Odessa",
                            "totals": "$612,905.03",
                            "lastYear": "$4,961.12",
                            "currentYear": "$15,247.24",
                            "ceo": "Irma Ingram",
                            "branches": [
                                {
                                    "id": 0,
                                    "name": "Lyrichord",
                                    "balance": "$4,313.39",
                                    "lastYear": "$187.99",
                                    "currentYear": "$498.97",
                                    "partner": true,
                                    "chief": "Whitney Powell",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Benita Christensen",
                                            "position": "Worker",
                                            "age": 43,
                                            "tall": 192,
                                            "weight": 62,
                                            "rating": 3,
                                            "salary": "$344.88"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Herminia Chase",
                                            "position": "Engineer",
                                            "age": 43,
                                            "tall": 184,
                                            "weight": 80,
                                            "rating": 2,
                                            "salary": "$419.29"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Cortez Rich",
                                            "position": "Gardener",
                                            "age": 29,
                                            "tall": 180,
                                            "weight": 97,
                                            "rating": 3,
                                            "salary": "$442.48"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Sellers Wade",
                                            "position": "Plumber",
                                            "age": 26,
                                            "tall": 154,
                                            "weight": 79,
                                            "rating": 3,
                                            "salary": "$463.78"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Fleming Grimes",
                                            "position": "Worker",
                                            "age": 46,
                                            "tall": 175,
                                            "weight": 101,
                                            "rating": 4,
                                            "salary": "$476.85"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Sosa Bright",
                                            "position": "Engineer",
                                            "age": 48,
                                            "tall": 158,
                                            "weight": 82,
                                            "rating": 2,
                                            "salary": "$375.37"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Bertie Rios",
                                            "position": "Worker",
                                            "age": 25,
                                            "tall": 167,
                                            "weight": 79,
                                            "rating": 7,
                                            "salary": "$413.51"
                                        }
                                    ]
                                },
                                {
                                    "id": 1,
                                    "name": "Ginkle",
                                    "balance": "$379.61",
                                    "lastYear": "$658.53",
                                    "currentYear": "$768.68",
                                    "partner": true,
                                    "chief": "Tucker Hubbard",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Spence Sherman",
                                            "position": "Gardener",
                                            "age": 39,
                                            "tall": 178,
                                            "weight": 76,
                                            "rating": 7,
                                            "salary": "$240.95"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Georgina Collier",
                                            "position": "Gardener",
                                            "age": 33,
                                            "tall": 182,
                                            "weight": 100,
                                            "rating": 7,
                                            "salary": "$318.23"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Conner Fitzgerald",
                                            "position": "Gardener",
                                            "age": 35,
                                            "tall": 184,
                                            "weight": 72,
                                            "rating": 5,
                                            "salary": "$347.19"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Grant Ross",
                                            "position": "Engineer",
                                            "age": 48,
                                            "tall": 187,
                                            "weight": 90,
                                            "rating": 6,
                                            "salary": "$233.65"
                                        }
                                    ]
                                },
                                {
                                    "id": 2,
                                    "name": "Diginetic",
                                    "balance": "$4,395.97",
                                    "lastYear": "$419.39",
                                    "currentYear": "$907.99",
                                    "partner": true,
                                    "chief": "Herring Webster",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Wilkerson Trujillo",
                                            "position": "Gardener",
                                            "age": 39,
                                            "tall": 169,
                                            "weight": 77,
                                            "rating": 5,
                                            "salary": "$355.69"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Foster Ochoa",
                                            "position": "Plumber",
                                            "age": 22,
                                            "tall": 169,
                                            "weight": 59,
                                            "rating": 3,
                                            "salary": "$203.85"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Browning Woodward",
                                            "position": "Worker",
                                            "age": 37,
                                            "tall": 182,
                                            "weight": 60,
                                            "rating": 5,
                                            "salary": "$317.93"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Lenore Hensley",
                                            "position": "Plumber",
                                            "age": 49,
                                            "tall": 191,
                                            "weight": 96,
                                            "rating": 9,
                                            "salary": "$475.00"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Riddle Burns",
                                            "position": "Electrician",
                                            "age": 28,
                                            "tall": 160,
                                            "weight": 70,
                                            "rating": 7,
                                            "salary": "$229.54"
                                        }
                                    ]
                                },
                                {
                                    "id": 3,
                                    "name": "Signity",
                                    "balance": "$4,240.13",
                                    "lastYear": "$797.69",
                                    "currentYear": "$518.27",
                                    "partner": false,
                                    "chief": "Castaneda Hewitt",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Janell Conner",
                                            "position": "Technician",
                                            "age": 50,
                                            "tall": 172,
                                            "weight": 64,
                                            "rating": 5,
                                            "salary": "$426.03"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Gilmore Lopez",
                                            "position": "Electrician",
                                            "age": 26,
                                            "tall": 175,
                                            "weight": 72,
                                            "rating": 4,
                                            "salary": "$230.06"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Higgins Reed",
                                            "position": "Electrician",
                                            "age": 24,
                                            "tall": 185,
                                            "weight": 102,
                                            "rating": 2,
                                            "salary": "$316.87"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Lora Hodge",
                                            "position": "Technician",
                                            "age": 33,
                                            "tall": 159,
                                            "weight": 61,
                                            "rating": 2,
                                            "salary": "$410.32"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Gallagher Frye",
                                            "position": "Gardener",
                                            "age": 45,
                                            "tall": 178,
                                            "weight": 62,
                                            "rating": 10,
                                            "salary": "$312.49"
                                        }
                                    ]
                                },
                                {
                                    "id": 4,
                                    "name": "Enersave",
                                    "balance": "$1,251.02",
                                    "lastYear": "$615.25",
                                    "currentYear": "$191.72",
                                    "partner": true,
                                    "chief": "Lara Parsons",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Kaye Ruiz",
                                            "position": "Plumber",
                                            "age": 46,
                                            "tall": 164,
                                            "weight": 86,
                                            "rating": 4,
                                            "salary": "$233.23"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Duran Alston",
                                            "position": "Plumber",
                                            "age": 24,
                                            "tall": 172,
                                            "weight": 65,
                                            "rating": 10,
                                            "salary": "$328.41"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Maureen Merrill",
                                            "position": "Plumber",
                                            "age": 19,
                                            "tall": 187,
                                            "weight": 59,
                                            "rating": 6,
                                            "salary": "$231.75"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Mays Padilla",
                                            "position": "Gardener",
                                            "age": 41,
                                            "tall": 176,
                                            "weight": 86,
                                            "rating": 6,
                                            "salary": "$415.71"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Sweet Ball",
                                            "position": "Engineer",
                                            "age": 37,
                                            "tall": 169,
                                            "weight": 63,
                                            "rating": 8,
                                            "salary": "$207.87"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Dominguez Salas",
                                            "position": "Gardener",
                                            "age": 31,
                                            "tall": 155,
                                            "weight": 96,
                                            "rating": 6,
                                            "salary": "$441.75"
                                        }
                                    ]
                                },
                                {
                                    "id": 5,
                                    "name": "Beadzza",
                                    "balance": "$2,017.08",
                                    "lastYear": "$253.35",
                                    "currentYear": "$565.25",
                                    "partner": true,
                                    "chief": "Miriam Armstrong",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Essie Nichols",
                                            "position": "Plumber",
                                            "age": 41,
                                            "tall": 160,
                                            "weight": 83,
                                            "rating": 5,
                                            "salary": "$325.39"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Dixon Patterson",
                                            "position": "Engineer",
                                            "age": 39,
                                            "tall": 180,
                                            "weight": 77,
                                            "rating": 7,
                                            "salary": "$293.74"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Elise Reyes",
                                            "position": "Electrician",
                                            "age": 46,
                                            "tall": 168,
                                            "weight": 63,
                                            "rating": 9,
                                            "salary": "$210.86"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Karen Spence",
                                            "position": "Electrician",
                                            "age": 20,
                                            "tall": 175,
                                            "weight": 78,
                                            "rating": 5,
                                            "salary": "$450.23"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Adrienne Hahn",
                                            "position": "Plumber",
                                            "age": 23,
                                            "tall": 164,
                                            "weight": 89,
                                            "rating": 2,
                                            "salary": "$396.58"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Mcdonald Chen",
                                            "position": "Technician",
                                            "age": 27,
                                            "tall": 181,
                                            "weight": 91,
                                            "rating": 6,
                                            "salary": "$455.59"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Francis Rose",
                                            "position": "Plumber",
                                            "age": 38,
                                            "tall": 166,
                                            "weight": 60,
                                            "rating": 3,
                                            "salary": "$306.42"
                                        },
                                        {
                                            "id": 7,
                                            "name": "Jan Knox",
                                            "position": "Plumber",
                                            "age": 49,
                                            "tall": 157,
                                            "weight": 94,
                                            "rating": 3,
                                            "salary": "$423.60"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "5c90b06371c5ac4143014a56",
                            "company": "Exoplode",
                            "city": "Hebron",
                            "totals": "$515,645.09",
                            "lastYear": "$17,042.08",
                            "currentYear": "$1,468.60",
                            "ceo": "Pierce Glenn",
                            "branches": [
                                {
                                    "id": 0,
                                    "name": "Isotronic",
                                    "balance": "$2,096.29",
                                    "lastYear": "$805.27",
                                    "currentYear": "$7.67",
                                    "partner": true,
                                    "chief": "Wall Hess",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Lavonne Gaines",
                                            "position": "Worker",
                                            "age": 40,
                                            "tall": 188,
                                            "weight": 91,
                                            "rating": 8,
                                            "salary": "$340.99"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Terrie Lott",
                                            "position": "Worker",
                                            "age": 20,
                                            "tall": 161,
                                            "weight": 66,
                                            "rating": 9,
                                            "salary": "$414.31"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Rocha Dale",
                                            "position": "Technician",
                                            "age": 25,
                                            "tall": 171,
                                            "weight": 83,
                                            "rating": 10,
                                            "salary": "$248.60"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Traci Hardy",
                                            "position": "Engineer",
                                            "age": 33,
                                            "tall": 155,
                                            "weight": 64,
                                            "rating": 3,
                                            "salary": "$338.74"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Shanna Mann",
                                            "position": "Plumber",
                                            "age": 37,
                                            "tall": 160,
                                            "weight": 100,
                                            "rating": 3,
                                            "salary": "$347.26"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Neal Collins",
                                            "position": "Plumber",
                                            "age": 21,
                                            "tall": 177,
                                            "weight": 56,
                                            "rating": 8,
                                            "salary": "$362.95"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Byrd Bird",
                                            "position": "Technician",
                                            "age": 33,
                                            "tall": 191,
                                            "weight": 91,
                                            "rating": 7,
                                            "salary": "$390.29"
                                        },
                                        {
                                            "id": 7,
                                            "name": "Atkins Everett",
                                            "position": "Plumber",
                                            "age": 35,
                                            "tall": 162,
                                            "weight": 95,
                                            "rating": 9,
                                            "salary": "$268.11"
                                        }
                                    ]
                                },
                                {
                                    "id": 1,
                                    "name": "Andershun",
                                    "balance": "$1,784.42",
                                    "lastYear": "$1.06",
                                    "currentYear": "$216.39",
                                    "partner": true,
                                    "chief": "Morin Mccarthy",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Graham Durham",
                                            "position": "Plumber",
                                            "age": 46,
                                            "tall": 192,
                                            "weight": 97,
                                            "rating": 3,
                                            "salary": "$414.36"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Roman Prince",
                                            "position": "Worker",
                                            "age": 33,
                                            "tall": 165,
                                            "weight": 63,
                                            "rating": 3,
                                            "salary": "$493.61"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Sadie Bauer",
                                            "position": "Gardener",
                                            "age": 46,
                                            "tall": 173,
                                            "weight": 99,
                                            "rating": 4,
                                            "salary": "$302.62"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Cecilia Kirkland",
                                            "position": "Technician",
                                            "age": 19,
                                            "tall": 156,
                                            "weight": 82,
                                            "rating": 7,
                                            "salary": "$329.53"
                                        }
                                    ]
                                },
                                {
                                    "id": 2,
                                    "name": "Combot",
                                    "balance": "$4,726.89",
                                    "lastYear": "$907.79",
                                    "currentYear": "$435.09",
                                    "partner": true,
                                    "chief": "Rochelle Johns",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Jenna William",
                                            "position": "Technician",
                                            "age": 36,
                                            "tall": 177,
                                            "weight": 81,
                                            "rating": 2,
                                            "salary": "$491.40"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Velma Paul",
                                            "position": "Gardener",
                                            "age": 44,
                                            "tall": 183,
                                            "weight": 99,
                                            "rating": 9,
                                            "salary": "$324.14"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Petersen Boyd",
                                            "position": "Plumber",
                                            "age": 29,
                                            "tall": 157,
                                            "weight": 91,
                                            "rating": 7,
                                            "salary": "$294.29"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Gay Barnett",
                                            "position": "Electrician",
                                            "age": 19,
                                            "tall": 167,
                                            "weight": 84,
                                            "rating": 8,
                                            "salary": "$283.91"
                                        }
                                    ]
                                },
                                {
                                    "id": 3,
                                    "name": "Pearlessa",
                                    "balance": "$3,190.14",
                                    "lastYear": "$959.43",
                                    "currentYear": "$836.26",
                                    "partner": false,
                                    "chief": "Barr Jacobs",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Maura Goff",
                                            "position": "Engineer",
                                            "age": 33,
                                            "tall": 189,
                                            "weight": 58,
                                            "rating": 8,
                                            "salary": "$324.34"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Ross Huber",
                                            "position": "Technician",
                                            "age": 40,
                                            "tall": 171,
                                            "weight": 72,
                                            "rating": 8,
                                            "salary": "$245.29"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Lucy Santos",
                                            "position": "Technician",
                                            "age": 35,
                                            "tall": 172,
                                            "weight": 65,
                                            "rating": 2,
                                            "salary": "$364.94"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Dona Lang",
                                            "position": "Electrician",
                                            "age": 22,
                                            "tall": 159,
                                            "weight": 55,
                                            "rating": 8,
                                            "salary": "$486.40"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Mae Ellis",
                                            "position": "Plumber",
                                            "age": 44,
                                            "tall": 168,
                                            "weight": 80,
                                            "rating": 9,
                                            "salary": "$274.20"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Bernard Burnett",
                                            "position": "Electrician",
                                            "age": 28,
                                            "tall": 182,
                                            "weight": 57,
                                            "rating": 2,
                                            "salary": "$407.44"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Ila Mooney",
                                            "position": "Engineer",
                                            "age": 44,
                                            "tall": 175,
                                            "weight": 67,
                                            "rating": 10,
                                            "salary": "$320.21"
                                        }
                                    ]
                                },
                                {
                                    "id": 4,
                                    "name": "Delphide",
                                    "balance": "$2,090.19",
                                    "lastYear": "$827.80",
                                    "currentYear": "$665.51",
                                    "partner": false,
                                    "chief": "Schneider Langley",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Taylor Ryan",
                                            "position": "Plumber",
                                            "age": 19,
                                            "tall": 177,
                                            "weight": 61,
                                            "rating": 6,
                                            "salary": "$451.75"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Gloria Gamble",
                                            "position": "Worker",
                                            "age": 18,
                                            "tall": 168,
                                            "weight": 95,
                                            "rating": 7,
                                            "salary": "$373.27"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Stokes Sharpe",
                                            "position": "Electrician",
                                            "age": 50,
                                            "tall": 187,
                                            "weight": 79,
                                            "rating": 4,
                                            "salary": "$263.35"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Lea Lambert",
                                            "position": "Worker",
                                            "age": 37,
                                            "tall": 169,
                                            "weight": 101,
                                            "rating": 4,
                                            "salary": "$449.20"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "5c90b0630d61c32db6fb0d76",
                            "company": "Senmei",
                            "city": "Edgewater",
                            "totals": "$398,411.94",
                            "lastYear": "$18,228.44",
                            "currentYear": "$15,776.74",
                            "ceo": "Stacie Tucker",
                            "branches": [
                                {
                                    "id": 0,
                                    "name": "Joviold",
                                    "balance": "$1,117.71",
                                    "lastYear": "$859.57",
                                    "currentYear": "$317.03",
                                    "partner": false,
                                    "chief": "Bridget Fox",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Brenda Oneal",
                                            "position": "Engineer",
                                            "age": 41,
                                            "tall": 184,
                                            "weight": 95,
                                            "rating": 8,
                                            "salary": "$408.40"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Perry Atkins",
                                            "position": "Plumber",
                                            "age": 33,
                                            "tall": 159,
                                            "weight": 86,
                                            "rating": 5,
                                            "salary": "$393.47"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Deann Andrews",
                                            "position": "Plumber",
                                            "age": 36,
                                            "tall": 176,
                                            "weight": 55,
                                            "rating": 2,
                                            "salary": "$490.22"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Mallory Shields",
                                            "position": "Technician",
                                            "age": 38,
                                            "tall": 157,
                                            "weight": 92,
                                            "rating": 4,
                                            "salary": "$262.87"
                                        }
                                    ]
                                },
                                {
                                    "id": 1,
                                    "name": "Plasto",
                                    "balance": "$4,805.43",
                                    "lastYear": "$790.08",
                                    "currentYear": "$994.59",
                                    "partner": false,
                                    "chief": "Eva Snyder",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Brianna Forbes",
                                            "position": "Electrician",
                                            "age": 18,
                                            "tall": 177,
                                            "weight": 73,
                                            "rating": 10,
                                            "salary": "$434.61"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Woods Wynn",
                                            "position": "Engineer",
                                            "age": 32,
                                            "tall": 179,
                                            "weight": 55,
                                            "rating": 8,
                                            "salary": "$287.63"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Jensen Graham",
                                            "position": "Electrician",
                                            "age": 42,
                                            "tall": 181,
                                            "weight": 92,
                                            "rating": 10,
                                            "salary": "$499.56"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Kathryn Barton",
                                            "position": "Gardener",
                                            "age": 49,
                                            "tall": 189,
                                            "weight": 63,
                                            "rating": 5,
                                            "salary": "$262.31"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Meyer Sears",
                                            "position": "Engineer",
                                            "age": 18,
                                            "tall": 154,
                                            "weight": 83,
                                            "rating": 8,
                                            "salary": "$483.16"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Leigh Burgess",
                                            "position": "Plumber",
                                            "age": 18,
                                            "tall": 173,
                                            "weight": 55,
                                            "rating": 5,
                                            "salary": "$475.38"
                                        }
                                    ]
                                },
                                {
                                    "id": 2,
                                    "name": "Geekmosis",
                                    "balance": "$1,797.24",
                                    "lastYear": "$334.88",
                                    "currentYear": "$897.18",
                                    "partner": true,
                                    "chief": "Joan Edwards",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Geraldine Moran",
                                            "position": "Plumber",
                                            "age": 34,
                                            "tall": 177,
                                            "weight": 74,
                                            "rating": 4,
                                            "salary": "$223.86"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Erika Mcdowell",
                                            "position": "Worker",
                                            "age": 42,
                                            "tall": 159,
                                            "weight": 98,
                                            "rating": 10,
                                            "salary": "$311.85"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Latonya Romero",
                                            "position": "Worker",
                                            "age": 46,
                                            "tall": 191,
                                            "weight": 85,
                                            "rating": 8,
                                            "salary": "$269.34"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Cathryn Grant",
                                            "position": "Electrician",
                                            "age": 41,
                                            "tall": 172,
                                            "weight": 82,
                                            "rating": 2,
                                            "salary": "$364.72"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Tami Gilbert",
                                            "position": "Gardener",
                                            "age": 44,
                                            "tall": 156,
                                            "weight": 55,
                                            "rating": 8,
                                            "salary": "$492.66"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Santiago Shepard",
                                            "position": "Worker",
                                            "age": 25,
                                            "tall": 185,
                                            "weight": 87,
                                            "rating": 8,
                                            "salary": "$438.52"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Alford Chan",
                                            "position": "Engineer",
                                            "age": 40,
                                            "tall": 174,
                                            "weight": 77,
                                            "rating": 9,
                                            "salary": "$388.53"
                                        },
                                        {
                                            "id": 7,
                                            "name": "Abby York",
                                            "position": "Gardener",
                                            "age": 31,
                                            "tall": 183,
                                            "weight": 73,
                                            "rating": 9,
                                            "salary": "$289.39"
                                        }
                                    ]
                                },
                                {
                                    "id": 3,
                                    "name": "Magneato",
                                    "balance": "$960.21",
                                    "lastYear": "$649.66",
                                    "currentYear": "$442.42",
                                    "partner": false,
                                    "chief": "Ashlee Olson",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Emily Humphrey",
                                            "position": "Gardener",
                                            "age": 20,
                                            "tall": 167,
                                            "weight": 70,
                                            "rating": 4,
                                            "salary": "$488.99"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Yvette Delacruz",
                                            "position": "Plumber",
                                            "age": 31,
                                            "tall": 159,
                                            "weight": 97,
                                            "rating": 9,
                                            "salary": "$427.49"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Mercedes Gross",
                                            "position": "Plumber",
                                            "age": 18,
                                            "tall": 165,
                                            "weight": 96,
                                            "rating": 10,
                                            "salary": "$455.52"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Alyssa Barlow",
                                            "position": "Plumber",
                                            "age": 45,
                                            "tall": 183,
                                            "weight": 98,
                                            "rating": 9,
                                            "salary": "$207.96"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Shannon Wells",
                                            "position": "Engineer",
                                            "age": 40,
                                            "tall": 171,
                                            "weight": 66,
                                            "rating": 8,
                                            "salary": "$385.31"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Marissa Byrd",
                                            "position": "Worker",
                                            "age": 18,
                                            "tall": 163,
                                            "weight": 102,
                                            "rating": 6,
                                            "salary": "$352.29"
                                        }
                                    ]
                                },
                                {
                                    "id": 4,
                                    "name": "Centice",
                                    "balance": "$2,085.12",
                                    "lastYear": "$557.86",
                                    "currentYear": "$638.44",
                                    "partner": false,
                                    "chief": "Christa Boyle",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Obrien Patrick",
                                            "position": "Engineer",
                                            "age": 18,
                                            "tall": 164,
                                            "weight": 57,
                                            "rating": 10,
                                            "salary": "$413.76"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Barrera Spears",
                                            "position": "Technician",
                                            "age": 40,
                                            "tall": 190,
                                            "weight": 59,
                                            "rating": 5,
                                            "salary": "$418.00"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Michelle Petersen",
                                            "position": "Electrician",
                                            "age": 47,
                                            "tall": 161,
                                            "weight": 86,
                                            "rating": 9,
                                            "salary": "$255.90"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Workman Beach",
                                            "position": "Engineer",
                                            "age": 47,
                                            "tall": 162,
                                            "weight": 74,
                                            "rating": 4,
                                            "salary": "$200.68"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Petra Compton",
                                            "position": "Gardener",
                                            "age": 29,
                                            "tall": 175,
                                            "weight": 99,
                                            "rating": 7,
                                            "salary": "$201.58"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Beverley Garcia",
                                            "position": "Gardener",
                                            "age": 31,
                                            "tall": 182,
                                            "weight": 60,
                                            "rating": 8,
                                            "salary": "$280.84"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Leonor Osborn",
                                            "position": "Engineer",
                                            "age": 32,
                                            "tall": 163,
                                            "weight": 84,
                                            "rating": 2,
                                            "salary": "$319.73"
                                        }
                                    ]
                                },
                                {
                                    "id": 5,
                                    "name": "Zytrac",
                                    "balance": "$308.04",
                                    "lastYear": "$693.81",
                                    "currentYear": "$939.79",
                                    "partner": false,
                                    "chief": "Jasmine Rodriguez",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Alissa Duke",
                                            "position": "Technician",
                                            "age": 24,
                                            "tall": 192,
                                            "weight": 56,
                                            "rating": 4,
                                            "salary": "$383.01"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Hannah Dean",
                                            "position": "Gardener",
                                            "age": 43,
                                            "tall": 173,
                                            "weight": 76,
                                            "rating": 10,
                                            "salary": "$311.97"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Juliet Kelly",
                                            "position": "Technician",
                                            "age": 33,
                                            "tall": 157,
                                            "weight": 64,
                                            "rating": 10,
                                            "salary": "$350.11"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Hughes Galloway",
                                            "position": "Plumber",
                                            "age": 27,
                                            "tall": 187,
                                            "weight": 85,
                                            "rating": 8,
                                            "salary": "$274.54"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Nelda Cardenas",
                                            "position": "Technician",
                                            "age": 33,
                                            "tall": 178,
                                            "weight": 80,
                                            "rating": 9,
                                            "salary": "$308.22"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Liza Sullivan",
                                            "position": "Engineer",
                                            "age": 20,
                                            "tall": 178,
                                            "weight": 65,
                                            "rating": 10,
                                            "salary": "$425.39"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Dollie Pollard",
                                            "position": "Engineer",
                                            "age": 38,
                                            "tall": 181,
                                            "weight": 82,
                                            "rating": 6,
                                            "salary": "$291.70"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "5c90b063c2ba7d21039fa4b9",
                            "company": "Geekus",
                            "city": "Glenbrook",
                            "totals": "$355,376.36",
                            "lastYear": "$7,623.12",
                            "currentYear": "$13,695.71",
                            "ceo": "Carolyn Leach",
                            "branches": [
                                {
                                    "id": 0,
                                    "name": "Pasturia",
                                    "balance": "$1,090.68",
                                    "lastYear": "$574.53",
                                    "currentYear": "$621.82",
                                    "partner": true,
                                    "chief": "Curry Hamilton",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Wilson Cooke",
                                            "position": "Technician",
                                            "age": 39,
                                            "tall": 166,
                                            "weight": 92,
                                            "rating": 9,
                                            "salary": "$478.80"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Celeste Wilcox",
                                            "position": "Worker",
                                            "age": 41,
                                            "tall": 185,
                                            "weight": 68,
                                            "rating": 10,
                                            "salary": "$280.77"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Kirsten Barrett",
                                            "position": "Electrician",
                                            "age": 27,
                                            "tall": 161,
                                            "weight": 68,
                                            "rating": 6,
                                            "salary": "$309.60"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Mooney Lewis",
                                            "position": "Gardener",
                                            "age": 46,
                                            "tall": 188,
                                            "weight": 65,
                                            "rating": 5,
                                            "salary": "$277.78"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Downs Stewart",
                                            "position": "Gardener",
                                            "age": 28,
                                            "tall": 169,
                                            "weight": 92,
                                            "rating": 8,
                                            "salary": "$406.77"
                                        }
                                    ]
                                },
                                {
                                    "id": 1,
                                    "name": "Proxsoft",
                                    "balance": "$4,670.12",
                                    "lastYear": "$151.87",
                                    "currentYear": "$254.42",
                                    "partner": true,
                                    "chief": "Mcclure Jimenez",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Griffith Lara",
                                            "position": "Engineer",
                                            "age": 27,
                                            "tall": 167,
                                            "weight": 102,
                                            "rating": 5,
                                            "salary": "$390.03"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Rosa Cooley",
                                            "position": "Engineer",
                                            "age": 29,
                                            "tall": 179,
                                            "weight": 64,
                                            "rating": 3,
                                            "salary": "$278.27"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Reyna Marshall",
                                            "position": "Electrician",
                                            "age": 31,
                                            "tall": 156,
                                            "weight": 56,
                                            "rating": 5,
                                            "salary": "$432.88"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Melissa Hopper",
                                            "position": "Plumber",
                                            "age": 37,
                                            "tall": 192,
                                            "weight": 100,
                                            "rating": 6,
                                            "salary": "$462.60"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Cecelia Mcguire",
                                            "position": "Technician",
                                            "age": 39,
                                            "tall": 174,
                                            "weight": 72,
                                            "rating": 7,
                                            "salary": "$340.21"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Brooks Moses",
                                            "position": "Engineer",
                                            "age": 21,
                                            "tall": 172,
                                            "weight": 62,
                                            "rating": 4,
                                            "salary": "$253.93"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Meyers Franks",
                                            "position": "Engineer",
                                            "age": 41,
                                            "tall": 163,
                                            "weight": 87,
                                            "rating": 9,
                                            "salary": "$248.94"
                                        }
                                    ]
                                },
                                {
                                    "id": 2,
                                    "name": "Prismatic",
                                    "balance": "$3,220.45",
                                    "lastYear": "$819.96",
                                    "currentYear": "$837.19",
                                    "partner": false,
                                    "chief": "Schroeder Livingston",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Ursula Crosby",
                                            "position": "Engineer",
                                            "age": 33,
                                            "tall": 175,
                                            "weight": 73,
                                            "rating": 4,
                                            "salary": "$355.38"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Esther Bartlett",
                                            "position": "Electrician",
                                            "age": 38,
                                            "tall": 175,
                                            "weight": 69,
                                            "rating": 6,
                                            "salary": "$223.58"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Juarez Roy",
                                            "position": "Engineer",
                                            "age": 49,
                                            "tall": 169,
                                            "weight": 80,
                                            "rating": 4,
                                            "salary": "$429.98"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Estela Wooten",
                                            "position": "Gardener",
                                            "age": 23,
                                            "tall": 167,
                                            "weight": 82,
                                            "rating": 6,
                                            "salary": "$282.99"
                                        }
                                    ]
                                },
                                {
                                    "id": 3,
                                    "name": "Architax",
                                    "balance": "$1,456.00",
                                    "lastYear": "$693.70",
                                    "currentYear": "$593.88",
                                    "partner": false,
                                    "chief": "Amy Hoffman",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Tate Roth",
                                            "position": "Worker",
                                            "age": 40,
                                            "tall": 168,
                                            "weight": 60,
                                            "rating": 8,
                                            "salary": "$274.56"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Constance Johnson",
                                            "position": "Plumber",
                                            "age": 20,
                                            "tall": 160,
                                            "weight": 56,
                                            "rating": 6,
                                            "salary": "$354.59"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Britney Mcpherson",
                                            "position": "Worker",
                                            "age": 34,
                                            "tall": 178,
                                            "weight": 89,
                                            "rating": 3,
                                            "salary": "$474.83"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Abigail Davenport",
                                            "position": "Plumber",
                                            "age": 39,
                                            "tall": 166,
                                            "weight": 78,
                                            "rating": 6,
                                            "salary": "$367.06"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Yvonne Meyer",
                                            "position": "Gardener",
                                            "age": 49,
                                            "tall": 170,
                                            "weight": 73,
                                            "rating": 10,
                                            "salary": "$380.16"
                                        }
                                    ]
                                },
                                {
                                    "id": 4,
                                    "name": "Lumbrex",
                                    "balance": "$2,916.53",
                                    "lastYear": "$374.54",
                                    "currentYear": "$971.09",
                                    "partner": true,
                                    "chief": "Wright Joyce",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Humphrey Medina",
                                            "position": "Technician",
                                            "age": 21,
                                            "tall": 191,
                                            "weight": 76,
                                            "rating": 3,
                                            "salary": "$398.84"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Alexandria Warner",
                                            "position": "Gardener",
                                            "age": 47,
                                            "tall": 159,
                                            "weight": 66,
                                            "rating": 4,
                                            "salary": "$311.79"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Long Coffey",
                                            "position": "Gardener",
                                            "age": 18,
                                            "tall": 191,
                                            "weight": 68,
                                            "rating": 2,
                                            "salary": "$362.50"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Emilia Rodriquez",
                                            "position": "Gardener",
                                            "age": 36,
                                            "tall": 177,
                                            "weight": 78,
                                            "rating": 5,
                                            "salary": "$356.44"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Short Shelton",
                                            "position": "Plumber",
                                            "age": 38,
                                            "tall": 191,
                                            "weight": 78,
                                            "rating": 5,
                                            "salary": "$298.56"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Elba Calhoun",
                                            "position": "Technician",
                                            "age": 34,
                                            "tall": 185,
                                            "weight": 70,
                                            "rating": 8,
                                            "salary": "$262.96"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Colon Joyner",
                                            "position": "Gardener",
                                            "age": 48,
                                            "tall": 161,
                                            "weight": 64,
                                            "rating": 3,
                                            "salary": "$248.53"
                                        },
                                        {
                                            "id": 7,
                                            "name": "Jacobs Bentley",
                                            "position": "Technician",
                                            "age": 40,
                                            "tall": 173,
                                            "weight": 79,
                                            "rating": 10,
                                            "salary": "$484.38"
                                        }
                                    ]
                                },
                                {
                                    "id": 5,
                                    "name": "Utarian",
                                    "balance": "$3,216.14",
                                    "lastYear": "$539.78",
                                    "currentYear": "$91.59",
                                    "partner": false,
                                    "chief": "Aida Baxter",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Wood Wilkins",
                                            "position": "Engineer",
                                            "age": 45,
                                            "tall": 183,
                                            "weight": 88,
                                            "rating": 7,
                                            "salary": "$238.28"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Benson Weaver",
                                            "position": "Worker",
                                            "age": 40,
                                            "tall": 168,
                                            "weight": 93,
                                            "rating": 3,
                                            "salary": "$236.39"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Reynolds Yates",
                                            "position": "Technician",
                                            "age": 43,
                                            "tall": 183,
                                            "weight": 59,
                                            "rating": 10,
                                            "salary": "$372.99"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Garrison Hickman",
                                            "position": "Engineer",
                                            "age": 48,
                                            "tall": 159,
                                            "weight": 99,
                                            "rating": 8,
                                            "salary": "$289.25"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Reilly Potter",
                                            "position": "Engineer",
                                            "age": 48,
                                            "tall": 174,
                                            "weight": 87,
                                            "rating": 8,
                                            "salary": "$381.77"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Rena Barnes",
                                            "position": "Engineer",
                                            "age": 19,
                                            "tall": 175,
                                            "weight": 96,
                                            "rating": 2,
                                            "salary": "$359.81"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Josephine Cox",
                                            "position": "Worker",
                                            "age": 43,
                                            "tall": 169,
                                            "weight": 97,
                                            "rating": 10,
                                            "salary": "$297.27"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "5c90b063e884b9ce94174bf1",
                            "company": "Paragonia",
                            "city": "Wildwood",
                            "totals": "$367,920.27",
                            "lastYear": "$978.97",
                            "currentYear": "$7,789.81",
                            "ceo": "Selma Bean",
                            "branches": [
                                {
                                    "id": 0,
                                    "name": "Rooforia",
                                    "balance": "$4,946.38",
                                    "lastYear": "$362.34",
                                    "currentYear": "$906.53",
                                    "partner": false,
                                    "chief": "Carrillo Mejia",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Tasha Juarez",
                                            "position": "Plumber",
                                            "age": 33,
                                            "tall": 169,
                                            "weight": 57,
                                            "rating": 9,
                                            "salary": "$270.40"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Helen Mason",
                                            "position": "Electrician",
                                            "age": 19,
                                            "tall": 161,
                                            "weight": 100,
                                            "rating": 5,
                                            "salary": "$347.37"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Mccarthy Bush",
                                            "position": "Worker",
                                            "age": 31,
                                            "tall": 162,
                                            "weight": 84,
                                            "rating": 8,
                                            "salary": "$408.48"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Mueller Sexton",
                                            "position": "Plumber",
                                            "age": 40,
                                            "tall": 192,
                                            "weight": 70,
                                            "rating": 2,
                                            "salary": "$412.35"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Karin Santiago",
                                            "position": "Gardener",
                                            "age": 47,
                                            "tall": 165,
                                            "weight": 80,
                                            "rating": 9,
                                            "salary": "$249.25"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Bender Whitney",
                                            "position": "Gardener",
                                            "age": 44,
                                            "tall": 186,
                                            "weight": 79,
                                            "rating": 7,
                                            "salary": "$237.32"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Joyce Dominguez",
                                            "position": "Electrician",
                                            "age": 23,
                                            "tall": 154,
                                            "weight": 93,
                                            "rating": 2,
                                            "salary": "$270.23"
                                        }
                                    ]
                                },
                                {
                                    "id": 1,
                                    "name": "Zilch",
                                    "balance": "$3,292.92",
                                    "lastYear": "$904.69",
                                    "currentYear": "$618.97",
                                    "partner": true,
                                    "chief": "Ellison Shepherd",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Farmer Byers",
                                            "position": "Plumber",
                                            "age": 31,
                                            "tall": 160,
                                            "weight": 91,
                                            "rating": 3,
                                            "salary": "$204.37"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Mayra Walls",
                                            "position": "Engineer",
                                            "age": 28,
                                            "tall": 161,
                                            "weight": 90,
                                            "rating": 3,
                                            "salary": "$225.31"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Bush Swanson",
                                            "position": "Gardener",
                                            "age": 31,
                                            "tall": 166,
                                            "weight": 66,
                                            "rating": 4,
                                            "salary": "$228.00"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Berger Church",
                                            "position": "Worker",
                                            "age": 31,
                                            "tall": 189,
                                            "weight": 66,
                                            "rating": 6,
                                            "salary": "$279.50"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Bean Hanson",
                                            "position": "Technician",
                                            "age": 25,
                                            "tall": 167,
                                            "weight": 88,
                                            "rating": 5,
                                            "salary": "$329.86"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Leola Lowery",
                                            "position": "Engineer",
                                            "age": 29,
                                            "tall": 167,
                                            "weight": 74,
                                            "rating": 2,
                                            "salary": "$494.39"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "5c90b0638a9d5f347808f169",
                            "company": "Intergeek",
                            "city": "Gordon",
                            "totals": "$250,572.38",
                            "lastYear": "$13,767.93",
                            "currentYear": "$7,519.11",
                            "ceo": "Becker Kidd",
                            "branches": [
                                {
                                    "id": 0,
                                    "name": "Bluegrain",
                                    "balance": "$822.35",
                                    "lastYear": "$69.28",
                                    "currentYear": "$556.77",
                                    "partner": false,
                                    "chief": "Andrews Gordon",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Julianne Buckner",
                                            "position": "Gardener",
                                            "age": 38,
                                            "tall": 175,
                                            "weight": 101,
                                            "rating": 5,
                                            "salary": "$439.68"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Marilyn Herrera",
                                            "position": "Worker",
                                            "age": 38,
                                            "tall": 159,
                                            "weight": 71,
                                            "rating": 2,
                                            "salary": "$207.54"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Rachael Duran",
                                            "position": "Plumber",
                                            "age": 26,
                                            "tall": 155,
                                            "weight": 69,
                                            "rating": 10,
                                            "salary": "$240.55"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Small Callahan",
                                            "position": "Engineer",
                                            "age": 20,
                                            "tall": 162,
                                            "weight": 77,
                                            "rating": 4,
                                            "salary": "$447.47"
                                        }
                                    ]
                                },
                                {
                                    "id": 1,
                                    "name": "Medifax",
                                    "balance": "$2,477.27",
                                    "lastYear": "$67.48",
                                    "currentYear": "$939.74",
                                    "partner": false,
                                    "chief": "Hazel Finley",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Deanna French",
                                            "position": "Electrician",
                                            "age": 46,
                                            "tall": 182,
                                            "weight": 81,
                                            "rating": 2,
                                            "salary": "$325.85"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Katina Maynard",
                                            "position": "Worker",
                                            "age": 24,
                                            "tall": 190,
                                            "weight": 74,
                                            "rating": 5,
                                            "salary": "$498.22"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Sylvia Peterson",
                                            "position": "Worker",
                                            "age": 49,
                                            "tall": 175,
                                            "weight": 70,
                                            "rating": 10,
                                            "salary": "$337.68"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Mercado Jensen",
                                            "position": "Engineer",
                                            "age": 21,
                                            "tall": 163,
                                            "weight": 79,
                                            "rating": 5,
                                            "salary": "$210.13"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Delacruz Mcneil",
                                            "position": "Worker",
                                            "age": 48,
                                            "tall": 186,
                                            "weight": 91,
                                            "rating": 4,
                                            "salary": "$397.73"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Olson Ortiz",
                                            "position": "Engineer",
                                            "age": 50,
                                            "tall": 172,
                                            "weight": 78,
                                            "rating": 2,
                                            "salary": "$264.09"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Odessa Bowman",
                                            "position": "Gardener",
                                            "age": 26,
                                            "tall": 178,
                                            "weight": 77,
                                            "rating": 3,
                                            "salary": "$310.78"
                                        },
                                        {
                                            "id": 7,
                                            "name": "Hall Bray",
                                            "position": "Technician",
                                            "age": 50,
                                            "tall": 165,
                                            "weight": 85,
                                            "rating": 4,
                                            "salary": "$307.21"
                                        }
                                    ]
                                },
                                {
                                    "id": 2,
                                    "name": "Isoswitch",
                                    "balance": "$2,838.37",
                                    "lastYear": "$498.27",
                                    "currentYear": "$938.21",
                                    "partner": true,
                                    "chief": "Zelma Griffith",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Atkinson Klein",
                                            "position": "Engineer",
                                            "age": 42,
                                            "tall": 170,
                                            "weight": 94,
                                            "rating": 9,
                                            "salary": "$258.31"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Henry Hutchinson",
                                            "position": "Gardener",
                                            "age": 32,
                                            "tall": 177,
                                            "weight": 77,
                                            "rating": 10,
                                            "salary": "$452.99"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Muriel Bonner",
                                            "position": "Plumber",
                                            "age": 46,
                                            "tall": 157,
                                            "weight": 100,
                                            "rating": 5,
                                            "salary": "$423.63"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Bradford Bishop",
                                            "position": "Gardener",
                                            "age": 18,
                                            "tall": 173,
                                            "weight": 69,
                                            "rating": 2,
                                            "salary": "$232.46"
                                        }
                                    ]
                                },
                                {
                                    "id": 3,
                                    "name": "Marketoid",
                                    "balance": "$2,524.35",
                                    "lastYear": "$2.39",
                                    "currentYear": "$961.29",
                                    "partner": true,
                                    "chief": "Chase Dennis",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Chaney Mccray",
                                            "position": "Engineer",
                                            "age": 50,
                                            "tall": 181,
                                            "weight": 71,
                                            "rating": 5,
                                            "salary": "$489.78"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Hays Olsen",
                                            "position": "Electrician",
                                            "age": 34,
                                            "tall": 187,
                                            "weight": 102,
                                            "rating": 2,
                                            "salary": "$295.40"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Julia Simon",
                                            "position": "Worker",
                                            "age": 48,
                                            "tall": 178,
                                            "weight": 64,
                                            "rating": 10,
                                            "salary": "$388.46"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Castillo Bridges",
                                            "position": "Engineer",
                                            "age": 27,
                                            "tall": 185,
                                            "weight": 90,
                                            "rating": 4,
                                            "salary": "$345.06"
                                        }
                                    ]
                                },
                                {
                                    "id": 4,
                                    "name": "Bleendot",
                                    "balance": "$2,416.11",
                                    "lastYear": "$111.02",
                                    "currentYear": "$61.04",
                                    "partner": true,
                                    "chief": "Cornelia Skinner",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Sharlene Valdez",
                                            "position": "Plumber",
                                            "age": 39,
                                            "tall": 168,
                                            "weight": 91,
                                            "rating": 6,
                                            "salary": "$473.17"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Letitia Lancaster",
                                            "position": "Engineer",
                                            "age": 28,
                                            "tall": 160,
                                            "weight": 57,
                                            "rating": 7,
                                            "salary": "$431.60"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Terri Nguyen",
                                            "position": "Technician",
                                            "age": 33,
                                            "tall": 175,
                                            "weight": 94,
                                            "rating": 4,
                                            "salary": "$485.13"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Vaughan Fulton",
                                            "position": "Plumber",
                                            "age": 27,
                                            "tall": 179,
                                            "weight": 67,
                                            "rating": 2,
                                            "salary": "$312.42"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Palmer Schmidt",
                                            "position": "Engineer",
                                            "age": 32,
                                            "tall": 187,
                                            "weight": 63,
                                            "rating": 8,
                                            "salary": "$368.69"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Rollins Gregory",
                                            "position": "Plumber",
                                            "age": 32,
                                            "tall": 183,
                                            "weight": 64,
                                            "rating": 9,
                                            "salary": "$256.76"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Clark Zamora",
                                            "position": "Technician",
                                            "age": 36,
                                            "tall": 173,
                                            "weight": 72,
                                            "rating": 4,
                                            "salary": "$234.64"
                                        },
                                        {
                                            "id": 7,
                                            "name": "Ada Gibson",
                                            "position": "Technician",
                                            "age": 32,
                                            "tall": 178,
                                            "weight": 90,
                                            "rating": 2,
                                            "salary": "$460.85"
                                        }
                                    ]
                                },
                                {
                                    "id": 5,
                                    "name": "Sureplex",
                                    "balance": "$3,546.99",
                                    "lastYear": "$295.61",
                                    "currentYear": "$322.28",
                                    "partner": true,
                                    "chief": "Greene Austin",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Florence Moss",
                                            "position": "Electrician",
                                            "age": 28,
                                            "tall": 176,
                                            "weight": 80,
                                            "rating": 3,
                                            "salary": "$327.59"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Jaime Guthrie",
                                            "position": "Gardener",
                                            "age": 26,
                                            "tall": 176,
                                            "weight": 77,
                                            "rating": 5,
                                            "salary": "$299.89"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Rosanna Warren",
                                            "position": "Engineer",
                                            "age": 44,
                                            "tall": 170,
                                            "weight": 57,
                                            "rating": 8,
                                            "salary": "$311.12"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Imelda Bennett",
                                            "position": "Electrician",
                                            "age": 49,
                                            "tall": 156,
                                            "weight": 80,
                                            "rating": 6,
                                            "salary": "$223.50"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "5c90b0633de61f0b4bebfdc6",
                            "company": "Applideck",
                            "city": "Coleville",
                            "totals": "$484,999.71",
                            "lastYear": "$10,234.81",
                            "currentYear": "$4,390.01",
                            "ceo": "Louise Cabrera",
                            "branches": [
                                {
                                    "id": 0,
                                    "name": "Zogak",
                                    "balance": "$4,909.91",
                                    "lastYear": "$848.41",
                                    "currentYear": "$787.83",
                                    "partner": false,
                                    "chief": "Arlene Kirk",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Janis Brennan",
                                            "position": "Engineer",
                                            "age": 28,
                                            "tall": 158,
                                            "weight": 102,
                                            "rating": 4,
                                            "salary": "$333.79"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Ines Moon",
                                            "position": "Engineer",
                                            "age": 20,
                                            "tall": 165,
                                            "weight": 79,
                                            "rating": 7,
                                            "salary": "$468.20"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Martin Wyatt",
                                            "position": "Technician",
                                            "age": 35,
                                            "tall": 176,
                                            "weight": 73,
                                            "rating": 4,
                                            "salary": "$272.77"
                                        },
                                        {
                                            "id": 3,
                                            "name": "England Vance",
                                            "position": "Electrician",
                                            "age": 21,
                                            "tall": 160,
                                            "weight": 80,
                                            "rating": 10,
                                            "salary": "$317.04"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Lorna Ayala",
                                            "position": "Worker",
                                            "age": 48,
                                            "tall": 169,
                                            "weight": 57,
                                            "rating": 8,
                                            "salary": "$385.28"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Roth Perry",
                                            "position": "Engineer",
                                            "age": 49,
                                            "tall": 159,
                                            "weight": 68,
                                            "rating": 10,
                                            "salary": "$440.55"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Debra Duffy",
                                            "position": "Electrician",
                                            "age": 44,
                                            "tall": 155,
                                            "weight": 91,
                                            "rating": 3,
                                            "salary": "$301.01"
                                        },
                                        {
                                            "id": 7,
                                            "name": "Alfreda Stephens",
                                            "position": "Gardener",
                                            "age": 50,
                                            "tall": 165,
                                            "weight": 77,
                                            "rating": 10,
                                            "salary": "$443.11"
                                        }
                                    ]
                                },
                                {
                                    "id": 1,
                                    "name": "Genekom",
                                    "balance": "$2,064.31",
                                    "lastYear": "$921.97",
                                    "currentYear": "$256.86",
                                    "partner": false,
                                    "chief": "Allie Alvarado",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Dodson Love",
                                            "position": "Electrician",
                                            "age": 38,
                                            "tall": 190,
                                            "weight": 99,
                                            "rating": 4,
                                            "salary": "$444.49"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Isabella Newton",
                                            "position": "Engineer",
                                            "age": 37,
                                            "tall": 185,
                                            "weight": 58,
                                            "rating": 9,
                                            "salary": "$445.47"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Jocelyn Stephenson",
                                            "position": "Worker",
                                            "age": 27,
                                            "tall": 176,
                                            "weight": 61,
                                            "rating": 7,
                                            "salary": "$429.52"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Nielsen Stevenson",
                                            "position": "Plumber",
                                            "age": 38,
                                            "tall": 175,
                                            "weight": 72,
                                            "rating": 7,
                                            "salary": "$404.77"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Horn Burks",
                                            "position": "Plumber",
                                            "age": 40,
                                            "tall": 158,
                                            "weight": 90,
                                            "rating": 4,
                                            "salary": "$401.74"
                                        }
                                    ]
                                },
                                {
                                    "id": 2,
                                    "name": "Cytrek",
                                    "balance": "$2,002.32",
                                    "lastYear": "$356.92",
                                    "currentYear": "$551.46",
                                    "partner": true,
                                    "chief": "Hampton Holloway",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Clara Montgomery",
                                            "position": "Engineer",
                                            "age": 43,
                                            "tall": 191,
                                            "weight": 85,
                                            "rating": 7,
                                            "salary": "$367.99"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Dee Beasley",
                                            "position": "Worker",
                                            "age": 46,
                                            "tall": 156,
                                            "weight": 98,
                                            "rating": 6,
                                            "salary": "$260.86"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Baldwin Peters",
                                            "position": "Technician",
                                            "age": 23,
                                            "tall": 175,
                                            "weight": 92,
                                            "rating": 9,
                                            "salary": "$378.28"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Sparks Castillo",
                                            "position": "Plumber",
                                            "age": 39,
                                            "tall": 188,
                                            "weight": 61,
                                            "rating": 10,
                                            "salary": "$217.84"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Randall Carney",
                                            "position": "Plumber",
                                            "age": 32,
                                            "tall": 172,
                                            "weight": 64,
                                            "rating": 5,
                                            "salary": "$223.03"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "5c90b063afc35e43c83357b0",
                            "company": "Snorus",
                            "city": "Takilma",
                            "totals": "$190,837.34",
                            "lastYear": "$16,091.08",
                            "currentYear": "$12,745.18",
                            "ceo": "Kasey Lyons",
                            "branches": [
                                {
                                    "id": 0,
                                    "name": "Securia",
                                    "balance": "$3,180.22",
                                    "lastYear": "$973.26",
                                    "currentYear": "$288.75",
                                    "partner": false,
                                    "chief": "Mcdaniel Harmon",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Cruz Bradford",
                                            "position": "Worker",
                                            "age": 36,
                                            "tall": 179,
                                            "weight": 91,
                                            "rating": 7,
                                            "salary": "$326.31"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Brock Pratt",
                                            "position": "Plumber",
                                            "age": 23,
                                            "tall": 176,
                                            "weight": 97,
                                            "rating": 9,
                                            "salary": "$432.60"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Patrick Battle",
                                            "position": "Engineer",
                                            "age": 43,
                                            "tall": 191,
                                            "weight": 95,
                                            "rating": 10,
                                            "salary": "$406.49"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Nanette Fuentes",
                                            "position": "Technician",
                                            "age": 49,
                                            "tall": 187,
                                            "weight": 84,
                                            "rating": 9,
                                            "salary": "$266.81"
                                        }
                                    ]
                                },
                                {
                                    "id": 1,
                                    "name": "Viagreat",
                                    "balance": "$2,915.89",
                                    "lastYear": "$808.28",
                                    "currentYear": "$427.48",
                                    "partner": true,
                                    "chief": "Pollard Jackson",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Valerie Miles",
                                            "position": "Engineer",
                                            "age": 26,
                                            "tall": 163,
                                            "weight": 76,
                                            "rating": 2,
                                            "salary": "$436.49"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Yates Witt",
                                            "position": "Technician",
                                            "age": 46,
                                            "tall": 178,
                                            "weight": 80,
                                            "rating": 9,
                                            "salary": "$342.62"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Vicky Gay",
                                            "position": "Plumber",
                                            "age": 32,
                                            "tall": 174,
                                            "weight": 74,
                                            "rating": 8,
                                            "salary": "$279.30"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Murphy Mccall",
                                            "position": "Engineer",
                                            "age": 34,
                                            "tall": 156,
                                            "weight": 69,
                                            "rating": 8,
                                            "salary": "$282.51"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Medina Odonnell",
                                            "position": "Gardener",
                                            "age": 39,
                                            "tall": 167,
                                            "weight": 62,
                                            "rating": 10,
                                            "salary": "$421.88"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Tracie Reeves",
                                            "position": "Engineer",
                                            "age": 50,
                                            "tall": 168,
                                            "weight": 99,
                                            "rating": 3,
                                            "salary": "$353.19"
                                        }
                                    ]
                                },
                                {
                                    "id": 2,
                                    "name": "Zentia",
                                    "balance": "$820.18",
                                    "lastYear": "$423.27",
                                    "currentYear": "$873.81",
                                    "partner": true,
                                    "chief": "Carrie Joseph",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Mccullough Combs",
                                            "position": "Engineer",
                                            "age": 35,
                                            "tall": 191,
                                            "weight": 68,
                                            "rating": 5,
                                            "salary": "$261.45"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Victoria Daniel",
                                            "position": "Electrician",
                                            "age": 30,
                                            "tall": 157,
                                            "weight": 68,
                                            "rating": 8,
                                            "salary": "$432.26"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Church Britt",
                                            "position": "Worker",
                                            "age": 40,
                                            "tall": 165,
                                            "weight": 91,
                                            "rating": 4,
                                            "salary": "$402.70"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Bernice Price",
                                            "position": "Technician",
                                            "age": 36,
                                            "tall": 182,
                                            "weight": 81,
                                            "rating": 5,
                                            "salary": "$322.53"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Ina Orr",
                                            "position": "Technician",
                                            "age": 49,
                                            "tall": 181,
                                            "weight": 63,
                                            "rating": 6,
                                            "salary": "$418.28"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Frieda Brock",
                                            "position": "Electrician",
                                            "age": 27,
                                            "tall": 192,
                                            "weight": 86,
                                            "rating": 6,
                                            "salary": "$226.45"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Kari Nicholson",
                                            "position": "Plumber",
                                            "age": 26,
                                            "tall": 177,
                                            "weight": 75,
                                            "rating": 10,
                                            "salary": "$382.06"
                                        }
                                    ]
                                },
                                {
                                    "id": 3,
                                    "name": "Opticall",
                                    "balance": "$4,610.87",
                                    "lastYear": "$8.09",
                                    "currentYear": "$305.86",
                                    "partner": true,
                                    "chief": "Beatriz Blackwell",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Holman Neal",
                                            "position": "Electrician",
                                            "age": 20,
                                            "tall": 181,
                                            "weight": 60,
                                            "rating": 8,
                                            "salary": "$398.88"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Lenora Henry",
                                            "position": "Plumber",
                                            "age": 41,
                                            "tall": 183,
                                            "weight": 61,
                                            "rating": 3,
                                            "salary": "$332.49"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Pena Richards",
                                            "position": "Plumber",
                                            "age": 23,
                                            "tall": 192,
                                            "weight": 64,
                                            "rating": 3,
                                            "salary": "$400.20"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Hoffman Holcomb",
                                            "position": "Engineer",
                                            "age": 23,
                                            "tall": 180,
                                            "weight": 74,
                                            "rating": 3,
                                            "salary": "$490.64"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Sexton Dillon",
                                            "position": "Engineer",
                                            "age": 49,
                                            "tall": 180,
                                            "weight": 88,
                                            "rating": 7,
                                            "salary": "$359.05"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "5c90b0635e8a36fbc534511b",
                            "company": "Isologix",
                            "city": "Dennard",
                            "totals": "$449,253.55",
                            "lastYear": "$8,637.24",
                            "currentYear": "$7,820.15",
                            "ceo": "Herman Farley",
                            "branches": [
                                {
                                    "id": 0,
                                    "name": "Knowlysis",
                                    "balance": "$2,663.62",
                                    "lastYear": "$594.71",
                                    "currentYear": "$321.45",
                                    "partner": true,
                                    "chief": "Bonita Gallegos",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Hurley Higgins",
                                            "position": "Engineer",
                                            "age": 47,
                                            "tall": 172,
                                            "weight": 58,
                                            "rating": 4,
                                            "salary": "$292.87"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Lynette Martinez",
                                            "position": "Engineer",
                                            "age": 42,
                                            "tall": 157,
                                            "weight": 87,
                                            "rating": 5,
                                            "salary": "$483.86"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Kim Patel",
                                            "position": "Technician",
                                            "age": 42,
                                            "tall": 192,
                                            "weight": 96,
                                            "rating": 10,
                                            "salary": "$252.40"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Alisha Perkins",
                                            "position": "Gardener",
                                            "age": 44,
                                            "tall": 170,
                                            "weight": 89,
                                            "rating": 8,
                                            "salary": "$433.79"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Sonja Frank",
                                            "position": "Technician",
                                            "age": 47,
                                            "tall": 159,
                                            "weight": 62,
                                            "rating": 6,
                                            "salary": "$334.26"
                                        }
                                    ]
                                },
                                {
                                    "id": 1,
                                    "name": "Netur",
                                    "balance": "$680.27",
                                    "lastYear": "$33.35",
                                    "currentYear": "$432.67",
                                    "partner": false,
                                    "chief": "Haney Guerra",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Erin Howe",
                                            "position": "Plumber",
                                            "age": 48,
                                            "tall": 165,
                                            "weight": 63,
                                            "rating": 2,
                                            "salary": "$225.65"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Singleton Morris",
                                            "position": "Technician",
                                            "age": 36,
                                            "tall": 187,
                                            "weight": 100,
                                            "rating": 6,
                                            "salary": "$258.52"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Lana Giles",
                                            "position": "Worker",
                                            "age": 46,
                                            "tall": 158,
                                            "weight": 63,
                                            "rating": 10,
                                            "salary": "$214.58"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Marguerite Flores",
                                            "position": "Gardener",
                                            "age": 19,
                                            "tall": 163,
                                            "weight": 63,
                                            "rating": 4,
                                            "salary": "$282.40"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Lelia Morse",
                                            "position": "Electrician",
                                            "age": 22,
                                            "tall": 190,
                                            "weight": 81,
                                            "rating": 10,
                                            "salary": "$465.74"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "5c90b0636d6624d8d4d47cb5",
                            "company": "Talkalot",
                            "city": "Cawood",
                            "totals": "$347,782.21",
                            "lastYear": "$12,464.32",
                            "currentYear": "$4,197.69",
                            "ceo": "Frances Navarro",
                            "branches": [
                                {
                                    "id": 0,
                                    "name": "Automon",
                                    "balance": "$4,573.22",
                                    "lastYear": "$177.65",
                                    "currentYear": "$464.85",
                                    "partner": false,
                                    "chief": "Marcia Pugh",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Pat Bender",
                                            "position": "Engineer",
                                            "age": 30,
                                            "tall": 163,
                                            "weight": 101,
                                            "rating": 8,
                                            "salary": "$295.86"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Sasha Sheppard",
                                            "position": "Plumber",
                                            "age": 34,
                                            "tall": 174,
                                            "weight": 89,
                                            "rating": 4,
                                            "salary": "$438.50"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Marina Daniels",
                                            "position": "Electrician",
                                            "age": 20,
                                            "tall": 189,
                                            "weight": 92,
                                            "rating": 7,
                                            "salary": "$473.89"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Wendy Pickett",
                                            "position": "Electrician",
                                            "age": 32,
                                            "tall": 175,
                                            "weight": 89,
                                            "rating": 7,
                                            "salary": "$229.98"
                                        }
                                    ]
                                },
                                {
                                    "id": 1,
                                    "name": "Ovium",
                                    "balance": "$2,750.56",
                                    "lastYear": "$911.71",
                                    "currentYear": "$927.38",
                                    "partner": false,
                                    "chief": "Addie Whitley",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Tran Eaton",
                                            "position": "Electrician",
                                            "age": 47,
                                            "tall": 170,
                                            "weight": 100,
                                            "rating": 9,
                                            "salary": "$432.12"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Lawanda Logan",
                                            "position": "Electrician",
                                            "age": 33,
                                            "tall": 162,
                                            "weight": 86,
                                            "rating": 4,
                                            "salary": "$343.39"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Bailey Benton",
                                            "position": "Worker",
                                            "age": 50,
                                            "tall": 157,
                                            "weight": 100,
                                            "rating": 10,
                                            "salary": "$483.72"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Patty Acevedo",
                                            "position": "Electrician",
                                            "age": 35,
                                            "tall": 169,
                                            "weight": 67,
                                            "rating": 5,
                                            "salary": "$440.75"
                                        }
                                    ]
                                },
                                {
                                    "id": 2,
                                    "name": "Opticon",
                                    "balance": "$1,150.32",
                                    "lastYear": "$358.66",
                                    "currentYear": "$422.40",
                                    "partner": true,
                                    "chief": "Loraine Velasquez",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Barker Douglas",
                                            "position": "Technician",
                                            "age": 23,
                                            "tall": 183,
                                            "weight": 58,
                                            "rating": 2,
                                            "salary": "$415.69"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Leann Kline",
                                            "position": "Engineer",
                                            "age": 47,
                                            "tall": 188,
                                            "weight": 56,
                                            "rating": 10,
                                            "salary": "$266.45"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Callie Garza",
                                            "position": "Worker",
                                            "age": 25,
                                            "tall": 169,
                                            "weight": 65,
                                            "rating": 5,
                                            "salary": "$456.06"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Jo Mills",
                                            "position": "Worker",
                                            "age": 40,
                                            "tall": 173,
                                            "weight": 89,
                                            "rating": 6,
                                            "salary": "$294.01"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Rhodes Dickerson",
                                            "position": "Gardener",
                                            "age": 29,
                                            "tall": 166,
                                            "weight": 85,
                                            "rating": 3,
                                            "salary": "$415.65"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Craft Carpenter",
                                            "position": "Gardener",
                                            "age": 19,
                                            "tall": 179,
                                            "weight": 87,
                                            "rating": 3,
                                            "salary": "$337.38"
                                        }
                                    ]
                                },
                                {
                                    "id": 3,
                                    "name": "Isonus",
                                    "balance": "$1,606.33",
                                    "lastYear": "$65.74",
                                    "currentYear": "$333.29",
                                    "partner": false,
                                    "chief": "Nguyen Watts",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Flynn Ballard",
                                            "position": "Gardener",
                                            "age": 30,
                                            "tall": 180,
                                            "weight": 82,
                                            "rating": 9,
                                            "salary": "$214.55"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Alston Long",
                                            "position": "Engineer",
                                            "age": 30,
                                            "tall": 161,
                                            "weight": 75,
                                            "rating": 4,
                                            "salary": "$329.94"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Mayer Franco",
                                            "position": "Plumber",
                                            "age": 25,
                                            "tall": 187,
                                            "weight": 94,
                                            "rating": 6,
                                            "salary": "$404.12"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Farley Mccoy",
                                            "position": "Electrician",
                                            "age": 25,
                                            "tall": 171,
                                            "weight": 96,
                                            "rating": 9,
                                            "salary": "$359.09"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Justine Reese",
                                            "position": "Engineer",
                                            "age": 21,
                                            "tall": 176,
                                            "weight": 92,
                                            "rating": 6,
                                            "salary": "$428.62"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Ayers Castaneda",
                                            "position": "Worker",
                                            "age": 31,
                                            "tall": 185,
                                            "weight": 100,
                                            "rating": 2,
                                            "salary": "$459.80"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Patti Francis",
                                            "position": "Engineer",
                                            "age": 32,
                                            "tall": 165,
                                            "weight": 80,
                                            "rating": 4,
                                            "salary": "$310.75"
                                        },
                                        {
                                            "id": 7,
                                            "name": "Ora Jenkins",
                                            "position": "Gardener",
                                            "age": 36,
                                            "tall": 160,
                                            "weight": 78,
                                            "rating": 9,
                                            "salary": "$297.49"
                                        }
                                    ]
                                },
                                {
                                    "id": 4,
                                    "name": "Geofarm",
                                    "balance": "$811.74",
                                    "lastYear": "$483.13",
                                    "currentYear": "$768.56",
                                    "partner": false,
                                    "chief": "Hanson Buchanan",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Cunningham Anderson",
                                            "position": "Plumber",
                                            "age": 48,
                                            "tall": 168,
                                            "weight": 82,
                                            "rating": 4,
                                            "salary": "$464.81"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Joyce Porter",
                                            "position": "Plumber",
                                            "age": 42,
                                            "tall": 176,
                                            "weight": 77,
                                            "rating": 5,
                                            "salary": "$487.48"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Dorthy Bryan",
                                            "position": "Technician",
                                            "age": 25,
                                            "tall": 181,
                                            "weight": 73,
                                            "rating": 9,
                                            "salary": "$489.26"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Mcgowan Holt",
                                            "position": "Technician",
                                            "age": 30,
                                            "tall": 160,
                                            "weight": 101,
                                            "rating": 4,
                                            "salary": "$459.37"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Adrian Rollins",
                                            "position": "Worker",
                                            "age": 33,
                                            "tall": 159,
                                            "weight": 92,
                                            "rating": 7,
                                            "salary": "$488.25"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Ferguson Rhodes",
                                            "position": "Technician",
                                            "age": 45,
                                            "tall": 154,
                                            "weight": 90,
                                            "rating": 3,
                                            "salary": "$201.58"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Armstrong Barron",
                                            "position": "Technician",
                                            "age": 39,
                                            "tall": 159,
                                            "weight": 74,
                                            "rating": 10,
                                            "salary": "$359.20"
                                        }
                                    ]
                                },
                                {
                                    "id": 5,
                                    "name": "Maxemia",
                                    "balance": "$2,964.46",
                                    "lastYear": "$849.66",
                                    "currentYear": "$665.80",
                                    "partner": false,
                                    "chief": "Gomez Charles",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Evangelina Justice",
                                            "position": "Engineer",
                                            "age": 36,
                                            "tall": 155,
                                            "weight": 95,
                                            "rating": 2,
                                            "salary": "$347.21"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Adela Wiggins",
                                            "position": "Plumber",
                                            "age": 47,
                                            "tall": 168,
                                            "weight": 81,
                                            "rating": 4,
                                            "salary": "$313.95"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Moss Morrow",
                                            "position": "Worker",
                                            "age": 20,
                                            "tall": 159,
                                            "weight": 63,
                                            "rating": 2,
                                            "salary": "$444.53"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Helga Estes",
                                            "position": "Plumber",
                                            "age": 33,
                                            "tall": 189,
                                            "weight": 97,
                                            "rating": 5,
                                            "salary": "$490.09"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "5c90b06374e80613e9436830",
                            "company": "Keeg",
                            "city": "Westphalia",
                            "totals": "$53,630.04",
                            "lastYear": "$13,389.21",
                            "currentYear": "$13,839.67",
                            "ceo": "Clemons Cannon",
                            "branches": [
                                {
                                    "id": 0,
                                    "name": "Otherside",
                                    "balance": "$1,776.36",
                                    "lastYear": "$704.42",
                                    "currentYear": "$731.31",
                                    "partner": true,
                                    "chief": "Daniel Fuller",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Twila Cash",
                                            "position": "Plumber",
                                            "age": 46,
                                            "tall": 172,
                                            "weight": 69,
                                            "rating": 2,
                                            "salary": "$400.43"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Patrice Barber",
                                            "position": "Technician",
                                            "age": 40,
                                            "tall": 155,
                                            "weight": 87,
                                            "rating": 5,
                                            "salary": "$490.19"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Luz Pope",
                                            "position": "Worker",
                                            "age": 26,
                                            "tall": 191,
                                            "weight": 78,
                                            "rating": 8,
                                            "salary": "$226.02"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Bird Short",
                                            "position": "Engineer",
                                            "age": 48,
                                            "tall": 191,
                                            "weight": 87,
                                            "rating": 6,
                                            "salary": "$353.77"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Marva Fields",
                                            "position": "Technician",
                                            "age": 48,
                                            "tall": 158,
                                            "weight": 87,
                                            "rating": 5,
                                            "salary": "$449.61"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Mccarty Boyer",
                                            "position": "Electrician",
                                            "age": 33,
                                            "tall": 179,
                                            "weight": 67,
                                            "rating": 9,
                                            "salary": "$210.81"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Macias Herring",
                                            "position": "Plumber",
                                            "age": 22,
                                            "tall": 159,
                                            "weight": 77,
                                            "rating": 3,
                                            "salary": "$337.33"
                                        },
                                        {
                                            "id": 7,
                                            "name": "Terry Snider",
                                            "position": "Electrician",
                                            "age": 47,
                                            "tall": 182,
                                            "weight": 68,
                                            "rating": 2,
                                            "salary": "$483.14"
                                        }
                                    ]
                                },
                                {
                                    "id": 1,
                                    "name": "Navir",
                                    "balance": "$6.96",
                                    "lastYear": "$225.01",
                                    "currentYear": "$549.96",
                                    "partner": false,
                                    "chief": "Hale Benjamin",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Watkins Dyer",
                                            "position": "Worker",
                                            "age": 23,
                                            "tall": 161,
                                            "weight": 70,
                                            "rating": 8,
                                            "salary": "$462.34"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Harrell Maddox",
                                            "position": "Gardener",
                                            "age": 41,
                                            "tall": 158,
                                            "weight": 58,
                                            "rating": 9,
                                            "salary": "$447.51"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Ester Little",
                                            "position": "Gardener",
                                            "age": 39,
                                            "tall": 181,
                                            "weight": 81,
                                            "rating": 2,
                                            "salary": "$355.62"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Castro Chapman",
                                            "position": "Electrician",
                                            "age": 49,
                                            "tall": 171,
                                            "weight": 56,
                                            "rating": 7,
                                            "salary": "$297.01"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Marian Rutledge",
                                            "position": "Technician",
                                            "age": 25,
                                            "tall": 169,
                                            "weight": 98,
                                            "rating": 10,
                                            "salary": "$245.99"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Susana Camacho",
                                            "position": "Engineer",
                                            "age": 46,
                                            "tall": 174,
                                            "weight": 55,
                                            "rating": 10,
                                            "salary": "$217.55"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Snider Floyd",
                                            "position": "Electrician",
                                            "age": 28,
                                            "tall": 186,
                                            "weight": 101,
                                            "rating": 3,
                                            "salary": "$355.49"
                                        },
                                        {
                                            "id": 7,
                                            "name": "Mckay Foster",
                                            "position": "Gardener",
                                            "age": 35,
                                            "tall": 170,
                                            "weight": 99,
                                            "rating": 2,
                                            "salary": "$343.94"
                                        }
                                    ]
                                },
                                {
                                    "id": 2,
                                    "name": "Isostream",
                                    "balance": "$2,367.26",
                                    "lastYear": "$513.48",
                                    "currentYear": "$347.14",
                                    "partner": false,
                                    "chief": "Latisha Odom",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Pickett Mcintosh",
                                            "position": "Electrician",
                                            "age": 30,
                                            "tall": 160,
                                            "weight": 62,
                                            "rating": 5,
                                            "salary": "$283.34"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Strickland Hatfield",
                                            "position": "Gardener",
                                            "age": 18,
                                            "tall": 185,
                                            "weight": 68,
                                            "rating": 4,
                                            "salary": "$449.74"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Hyde Franklin",
                                            "position": "Worker",
                                            "age": 38,
                                            "tall": 190,
                                            "weight": 64,
                                            "rating": 3,
                                            "salary": "$376.19"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Weber Travis",
                                            "position": "Worker",
                                            "age": 24,
                                            "tall": 188,
                                            "weight": 99,
                                            "rating": 4,
                                            "salary": "$279.01"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Anne Morin",
                                            "position": "Gardener",
                                            "age": 26,
                                            "tall": 171,
                                            "weight": 76,
                                            "rating": 10,
                                            "salary": "$335.79"
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            "id": "5c90b0637ecffec84a682874",
                            "company": "Singavera",
                            "city": "Orason",
                            "totals": "$868,059.33",
                            "lastYear": "$6,399.82",
                            "currentYear": "$9,465.08",
                            "ceo": "Schmidt Bates",
                            "branches": [
                                {
                                    "id": 0,
                                    "name": "Eclipsent",
                                    "balance": "$3,700.89",
                                    "lastYear": "$380.79",
                                    "currentYear": "$222.93",
                                    "partner": false,
                                    "chief": "Hensley Keith",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Angelia Woods",
                                            "position": "Worker",
                                            "age": 31,
                                            "tall": 174,
                                            "weight": 72,
                                            "rating": 8,
                                            "salary": "$399.15"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Velasquez Nelson",
                                            "position": "Gardener",
                                            "age": 42,
                                            "tall": 154,
                                            "weight": 68,
                                            "rating": 7,
                                            "salary": "$295.39"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Bonner Strickland",
                                            "position": "Gardener",
                                            "age": 25,
                                            "tall": 162,
                                            "weight": 88,
                                            "rating": 7,
                                            "salary": "$316.14"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Snow Lindsey",
                                            "position": "Engineer",
                                            "age": 50,
                                            "tall": 182,
                                            "weight": 94,
                                            "rating": 4,
                                            "salary": "$243.62"
                                        }
                                    ]
                                },
                                {
                                    "id": 1,
                                    "name": "Farmex",
                                    "balance": "$2,445.10",
                                    "lastYear": "$89.30",
                                    "currentYear": "$570.24",
                                    "partner": false,
                                    "chief": "Anita Mccormick",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Shawn Cameron",
                                            "position": "Plumber",
                                            "age": 24,
                                            "tall": 172,
                                            "weight": 75,
                                            "rating": 9,
                                            "salary": "$258.31"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Cheryl Rivas",
                                            "position": "Gardener",
                                            "age": 47,
                                            "tall": 171,
                                            "weight": 88,
                                            "rating": 9,
                                            "salary": "$221.10"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Berg Morton",
                                            "position": "Electrician",
                                            "age": 43,
                                            "tall": 178,
                                            "weight": 97,
                                            "rating": 3,
                                            "salary": "$223.70"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Ortiz Best",
                                            "position": "Electrician",
                                            "age": 41,
                                            "tall": 185,
                                            "weight": 84,
                                            "rating": 3,
                                            "salary": "$329.21"
                                        }
                                    ]
                                },
                                {
                                    "id": 2,
                                    "name": "Twiggery",
                                    "balance": "$3,121.45",
                                    "lastYear": "$803.05",
                                    "currentYear": "$763.23",
                                    "partner": false,
                                    "chief": "Ana Pearson",
                                    "people": [
                                        {
                                            "id": 0,
                                            "name": "Maria Burch",
                                            "position": "Gardener",
                                            "age": 44,
                                            "tall": 165,
                                            "weight": 94,
                                            "rating": 6,
                                            "salary": "$302.00"
                                        },
                                        {
                                            "id": 1,
                                            "name": "Mabel Washington",
                                            "position": "Electrician",
                                            "age": 24,
                                            "tall": 182,
                                            "weight": 92,
                                            "rating": 8,
                                            "salary": "$337.88"
                                        },
                                        {
                                            "id": 2,
                                            "name": "Tammi Burke",
                                            "position": "Worker",
                                            "age": 42,
                                            "tall": 163,
                                            "weight": 57,
                                            "rating": 2,
                                            "salary": "$368.47"
                                        },
                                        {
                                            "id": 3,
                                            "name": "Stafford Cunningham",
                                            "position": "Technician",
                                            "age": 24,
                                            "tall": 156,
                                            "weight": 77,
                                            "rating": 4,
                                            "salary": "$287.97"
                                        },
                                        {
                                            "id": 4,
                                            "name": "Noemi Dodson",
                                            "position": "Plumber",
                                            "age": 18,
                                            "tall": 176,
                                            "weight": 75,
                                            "rating": 10,
                                            "salary": "$388.68"
                                        },
                                        {
                                            "id": 5,
                                            "name": "Kimberly Tanner",
                                            "position": "Worker",
                                            "age": 18,
                                            "tall": 180,
                                            "weight": 85,
                                            "rating": 8,
                                            "salary": "$270.40"
                                        },
                                        {
                                            "id": 6,
                                            "name": "Opal Monroe",
                                            "position": "Electrician",
                                            "age": 32,
                                            "tall": 192,
                                            "weight": 77,
                                            "rating": 9,
                                            "salary": "$482.65"
                                        },
                                        {
                                            "id": 7,
                                            "name": "Rebekah Marquez",
                                            "position": "Worker",
                                            "age": 33,
                                            "tall": 171,
                                            "weight": 62,
                                            "rating": 10,
                                            "salary": "$244.64"
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                );
            })
        };

        return CompaniesData;

    });