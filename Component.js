sap.ui.define(['sap/ui/core/UIComponent', 'suek/test2/model/CompaniesData'],
    function(UIComponent, CompaniesData) {
    "use strict";

        var Component = UIComponent.extend("suek.test2.Component", {
            metadata : {

                manifest: "json"

            },

            /**
             * Инициализация компонента
             */
            init: function () {
                UIComponent.prototype.init && UIComponent.prototype.init.apply(this, arguments);

                this.initData();
            },

            /**
             * Инициализация данных
             * После инициализации данных иницианилизируем роутер
             */
            initData() {
                var oModel = this.getModel();
                new CompaniesData().read().then(function (data) {
                    var totals = {
                        companiesCount: data.length,
                        lastYear: 0,
                        currentYear: 0,
                        totals: 0
                    };
                    oModel.setData({
                        companies: data,
                        searchText: "",
                        tabConf: {
                            name: true,
                            age: true,
                            tall: true,
                            weight: true,
                            rating: true,
                            salary: true
                        }
                    });
                    data.forEach(function(company){
                        totals.lastYear += Number(company.lastYear.replace(',', '').replace('$',''));
                        totals.currentYear += Number(company.currentYear.replace(',', '').replace('$',''));
                        totals.totals += Number(company.totals.replace(',', '').replace('$',''));
                    });
                    oModel.setProperty('/totals', totals);
                    this.getRouter().initialize();
                }.bind(this));
            }

        });

        return Component;

    });